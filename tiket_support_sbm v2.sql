-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: tiket_support_sbm
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.22-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `admin` (
  `admin_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin',7,'2021-10-02 16:00:00','2021-10-02 16:00:00');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_layanan`
--

DROP TABLE IF EXISTS `admin_layanan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `admin_layanan` (
  `admin_id` int(11) NOT NULL,
  `layanan_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_layanan`
--

LOCK TABLES `admin_layanan` WRITE;
/*!40000 ALTER TABLE `admin_layanan` DISABLE KEYS */;
INSERT INTO `admin_layanan` VALUES (1,1,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(1,2,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(1,3,'2021-10-02 16:00:00','2021-10-02 16:00:00');
/*!40000 ALTER TABLE `admin_layanan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `comment` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tiket_id` int(11) DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lampiran` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,19,'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.','-',18,'2021-12-16 08:26:59','2021-12-16 08:26:59'),(2,19,'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.','file_comment/19-1639643261.pdf',18,'2021-12-16 08:27:41','2021-12-16 08:27:41'),(3,19,'Home 8','-',18,'2021-12-16 08:28:15','2021-12-16 08:28:15'),(4,19,'Home 7','-',18,'2021-12-16 08:28:19','2021-12-16 08:28:19'),(5,19,'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.','file_comment/19-1639643311.png',18,'2021-12-16 08:28:31','2021-12-16 08:28:31'),(6,19,'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.','file_comment/19-1639644962.docx',18,'2021-12-16 08:56:02','2021-12-16 08:56:02'),(7,19,'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.','-',18,'2021-12-16 08:58:19','2021-12-16 08:58:19'),(8,19,'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.','-',13,'2021-12-17 14:05:47','2021-12-17 14:05:47'),(9,19,'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.','file_comment/19-1639754481.jpeg',18,'2021-12-17 15:21:21','2021-12-17 15:21:21'),(10,24,'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.','-',18,'2021-12-21 00:55:59','2021-12-21 00:55:59'),(11,19,'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.','-',18,'2021-12-21 12:44:44','2021-12-21 12:44:44'),(12,19,'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.','-',18,'2021-12-21 12:45:25','2021-12-21 12:45:25'),(13,19,'I would like to meet you to discuss the latest news about the arrival of the new theme. They say it is going to be one the best themes on the market','-',13,'2021-12-21 12:46:44','2021-12-21 12:46:44'),(14,19,'I would like to meet you to discuss the latest news about the arrival of the new theme. They say it is going to be one the best themes on the market','-',13,'2021-12-21 12:48:28','2021-12-21 12:48:28'),(15,22,'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.','-',18,'2021-12-21 13:26:01','2021-12-21 13:26:01');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `klien`
--

DROP TABLE IF EXISTS `klien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `klien` (
  `klien_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`klien_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `klien`
--

LOCK TABLES `klien` WRITE;
/*!40000 ALTER TABLE `klien` DISABLE KEYS */;
INSERT INTO `klien` VALUES (1,'Amik','Denpasar Bali','083115411787',13,'2021-09-25 16:00:00','2021-09-25 16:00:00'),(2,'Ksusedana','Denpasar, Bali','083221467726',14,'2021-09-25 16:00:00','2021-09-25 16:00:00'),(3,'Yoga','Denpasar, Bali','0817263888',15,'2021-09-25 16:00:00','2021-09-25 16:00:00'),(4,'Ayu','Denpasar','087652890788',11,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(5,'Agus','Tabanan','08172891881',12,'2021-10-02 16:00:00','2021-10-02 16:00:00');
/*!40000 ALTER TABLE `klien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `klien_layanan`
--

DROP TABLE IF EXISTS `klien_layanan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `klien_layanan` (
  `layanan_id` int(11) NOT NULL,
  `klien_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `klien_id` (`klien_id`),
  KEY `layanan_id` (`layanan_id`),
  KEY `klien_layanan_klien_id_unique` (`klien_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `klien_layanan`
--

LOCK TABLES `klien_layanan` WRITE;
/*!40000 ALTER TABLE `klien_layanan` DISABLE KEYS */;
INSERT INTO `klien_layanan` VALUES (1,1,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(2,1,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(3,2,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(1,3,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(3,3,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(4,2,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(4,3,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(5,1,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(5,3,'2021-10-02 16:00:00','2021-10-02 16:00:00');
/*!40000 ALTER TABLE `klien_layanan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `layanan`
--

DROP TABLE IF EXISTS `layanan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `layanan` (
  `layanan_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis` int(11) NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`layanan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `layanan`
--

LOCK TABLES `layanan` WRITE;
/*!40000 ALTER TABLE `layanan` DISABLE KEYS */;
INSERT INTO `layanan` VALUES (1,'Pandana',1,'Sistem Koperasi','2021-09-25 16:00:00','2021-09-25 16:00:00'),(2,'Sirenbangda',2,'Sistem Perencanaan','2021-09-25 16:00:00','2021-09-25 16:00:00'),(3,'Sisenso',2,'Sistem Senso','2021-09-25 16:00:00','2021-09-25 16:00:00');
/*!40000 ALTER TABLE `layanan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2021_09_24_151708_create_tiket_status_table',1),(5,'2021_09_24_151754_create_tiket_jenis_table',1),(6,'2021_09_24_151841_create_tiket_foto_table',1),(7,'2021_09_24_151918_create_tiket_table',1),(8,'2021_09_24_151940_create_comment_table',1),(9,'2021_09_24_151957_create_pegawai_table',1),(10,'2021_09_24_152013_create_replies_table',1),(11,'2021_09_24_152036_create_pegawai_layanan_table',1),(12,'2021_09_24_152055_create_layanan_table',1),(13,'2021_09_24_152110_create_klien_layanan_table',1),(14,'2021_09_24_152129_create_klien_table',1),(15,'2021_09_24_152144_create_tagihan_table',1),(16,'2021_09_24_152209_create_pengembangan_table',1),(17,'2021_09_24_152231_create_review_table',1),(18,'2021_09_25_162000_create_tiket_pesan_table',1),(19,'2021_09_25_162223_create_tiket_log_table',1),(20,'2021_09_25_162534_create_roles_table',1),(21,'2021_09_25_163620_add_foto',1),(22,'2021_09_25_164348_create_selesai_table',1),(23,'2021_09_25_164451_create_tiket_diproses_table',1),(24,'2021_09_25_164521_create_tiket_status_diproses_table',1),(25,'2021_10_03_065011_create_admin_table',2),(26,'2021_10_03_065513_create_admin_layanan_table',2),(27,'2021_10_31_085015_add_kategori_to_tiket_table',3),(28,'2021_12_08_154619_create_tiket_pegawai_table',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifikasi`
--

DROP TABLE IF EXISTS `notifikasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `notifikasi` (
  `notif_id` char(20) NOT NULL,
  `user_id` int(11) NOT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `redirect_link` varchar(100) DEFAULT NULL,
  `dibaca` enum('BELUM','SUDAH') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`notif_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifikasi`
--

LOCK TABLES `notifikasi` WRITE;
/*!40000 ALTER TABLE `notifikasi` DISABLE KEYS */;
INSERT INTO `notifikasi` VALUES ('10F485D695805E591E30',18,'Komentar Pengaduan','Teknisi1 memberikan komentar pada Pengaduan diproses (komentar: It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.)','pegawai/pengaduan/diproses/detail/19','SUDAH','2021-12-21 12:44:44','2021-12-21 13:02:24'),('1834706CFAFD6B23D3DE',13,'Pengaduan selesai','Pengaduan Anda telah selesai diproses, silahkan berikan review pada Pengaduan ini (judul: diproses)','klien/selesai/detail/1/19','SUDAH','2021-12-21 12:55:40','2021-12-21 12:55:57'),('1B1FCC0D4FA099A8BD16',13,'Tagihan Pengaduan','Terdapat tagihan pada Pengaduan Jadwal Pembelajaran Online Tahun Pelajaran 2020-2021 (jumlah tagihan: Rp 1.200.000) silahkan lakukan pembayaran sebelum tanggal 28 Desember 2021','klien/diproses/detail/1/22','SUDAH','2021-12-21 13:02:15','2021-12-21 13:02:41'),('20A8F4E74249345E92D5',13,'Pengaduan diproses','Pengaduan Anda ditangani oleh teknisi1 (judul: Jadwal Pembelajaran Online Tahun Pelajaran 2020-2021)','klien/diproses/detail/1/26','SUDAH','2021-12-21 04:07:33','2021-12-21 04:11:57'),('275DE9AAF07908FA4908',18,'Pengembangan Baru','Terdapat Pengembangan baru layanan Sirenbangda dari klien Amik (judul: Ebook Budidaya Tanaman Mint Dalam Polybag Atau Pot)','pegawai/pengembangan/menunggu/2','SUDAH','2021-12-21 01:58:08','2021-12-21 13:02:24'),('275DE9AAF07908FA4908',19,'Pengembangan Baru','Terdapat Pengembangan baru layanan Sirenbangda dari klien Amik (judul: Ebook Budidaya Tanaman Mint Dalam Polybag Atau Pot)','pegawai/pengembangan/menunggu/2','BELUM','2021-12-21 01:58:08','2021-12-21 01:58:08'),('3A3EC1DEBCF1F9104825',18,'Review Pengaduan','Klien memberikan review dislike pada Pengaduan Jadwal Pembelajaran Online Tahun Pelajaran 2020-2021','pegawai/pengaduan/selesai/detail/26','SUDAH','2021-12-21 12:54:15','2021-12-21 13:02:24'),('4C5C4A66E289D72DF645',18,'Review Pengaduan','Klien memberikan review like pada Pengaduan diproses','pegawai/pengaduan/selesai/detail/19','SUDAH','2021-12-21 12:56:02','2021-12-21 13:02:24'),('4D3D06796BFFBD89B9DD',13,'Pengaduan diproses','Progress Pengaduan Anda 50% (judul: diproses)','klien/diproses/detail/1/19','SUDAH','2021-12-21 04:20:51','2021-12-21 04:21:13'),('5FBCEB4D3DD19BBDC6CE',13,'Komentar Pengaduan','Teknisi1 memberikan komentar pada Pengaduan diproses (komentar: It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.)','pegawai/pengaduan/diproses/detail/19','SUDAH','2021-12-21 12:45:25','2021-12-21 12:48:22'),('678C906E01D11DF8C20C',18,'Review Pengaduan','Klien memberikan review like pada Pengaduan eror','pegawai/pengaduan/selesai/detail/18','SUDAH','2021-12-21 12:54:09','2021-12-21 13:02:24'),('69223F5FF46CD5651C4F',18,'Pengaduan Baru','Terdapat Pengaduan baru layanan Pandana dari klien Amik (judul: Ebook Budidaya Tanaman Mint Dalam Polybag Atau Pot)','pegawai/pengaduan/menunggu/1','SUDAH','2021-12-21 04:00:54','2021-12-21 13:02:24'),('69223F5FF46CD5651C4F',19,'Pengaduan Baru','Terdapat Pengaduan baru layanan Pandana dari klien Amik (judul: Ebook Budidaya Tanaman Mint Dalam Polybag Atau Pot)','pegawai/pengaduan/menunggu/1','BELUM','2021-12-21 04:00:54','2021-12-21 04:00:54'),('75DE13B5D75BCA220D84',13,'Pengaduan diproses','Pengaduan Anda ditangani oleh Teknisi1 (judul: Jadwal Pembelajaran Online Tahun Pelajaran 2020-2021)','klien/diproses/detail/1/22','SUDAH','2021-12-21 12:58:39','2021-12-21 13:02:47'),('9815B7A2E4AC35AEA15D',13,'Komentar Pengaduan','Teknisi memberikan komentar pada Pengaduan Jadwal Pembelajaran Online Tahun Pelajaran 2020-2021 (komentar: It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.)','pegawai/pengaduan/diproses/detail/22','BELUM','2021-12-21 13:26:01','2021-12-21 13:26:01'),('9DB3610FE3C462CA83B3',19,'Pengaduan Baru','Terdapat Pengaduan baru layanan Pandana dari klien Amik (judul: Jadwal Pembelajaran Online Tahun Pelajaran 2020-2021)','pegawai/pengaduan/menunggu/1','BELUM','2021-12-21 01:56:53','2021-12-21 01:56:53'),('A9E871D8E9DED50DEDAA',13,'Pengaduan selesai','Pengaduan Anda telah selesai diproses (judul: Ebook Budidaya Tanaman Mint Dalam Polybag Atau Pot)','klien/selesai/detail/1/28','SUDAH','2021-12-21 04:21:50','2021-12-21 13:03:49'),('BC150EE9B788A6D5B965',18,'Komentar Pengaduan','Teknisi1 memberikan komentar pada Pengaduan diproses (komentar: I would like to meet you to discuss the latest news about the arrival of the new theme. They say it is going to be one the best themes on the market)','klien/diproses/detail/1/19','SUDAH','2021-12-21 12:46:44','2021-12-21 13:02:24'),('EDEB4289ED188912F45C',18,'Komentar Pengaduan','Amik memberikan komentar pada Pengaduan diproses (komentar: I would like to meet you to discuss the latest news about the arrival of the new theme. They say it is going to be one the best themes on the market)','klien/diproses/detail/1/19','SUDAH','2021-12-21 12:48:28','2021-12-21 13:02:24'),('FB384D4E6058F454C164',18,'Review Pengembangan','Klien memberikan review dislike pada Pengembangan Jadwal Pembelajaran Online Tahun Pelajaran 2020-2021','pegawai/pengembangan/selesai/detail/24','BELUM','2021-12-21 13:03:04','2021-12-21 13:03:04');
/*!40000 ALTER TABLE `notifikasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pegawai`
--

DROP TABLE IF EXISTS `pegawai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pegawai` (
  `pegawai_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pegawai_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pegawai`
--

LOCK TABLES `pegawai` WRITE;
/*!40000 ALTER TABLE `pegawai` DISABLE KEYS */;
INSERT INTO `pegawai` VALUES (1,'Teknisi1',18,'2021-09-25 16:00:00','2021-09-25 16:00:00'),(2,'Teknisi2',19,'2021-09-25 16:00:00','2021-09-25 16:00:00'),(3,'Teknisi3',3,'2021-09-25 16:00:00','2021-09-25 16:00:00'),(4,'Teknisi4',4,'2021-09-25 16:00:00','2021-09-25 16:00:00'),(5,'Teknisi5',5,'2021-09-25 16:00:00','2021-09-25 16:00:00'),(6,'Teknisi6',6,'2021-09-25 16:00:00','2021-09-25 16:00:00');
/*!40000 ALTER TABLE `pegawai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pegawai_layanan`
--

DROP TABLE IF EXISTS `pegawai_layanan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pegawai_layanan` (
  `pegawai_id` int(11) NOT NULL,
  `layanan_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pegawai_layanan`
--

LOCK TABLES `pegawai_layanan` WRITE;
/*!40000 ALTER TABLE `pegawai_layanan` DISABLE KEYS */;
INSERT INTO `pegawai_layanan` VALUES (1,1,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(1,2,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(2,3,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(2,2,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(3,1,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(3,3,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(4,2,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(4,1,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(5,1,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(5,3,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(6,1,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(6,2,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(6,3,'2021-10-02 16:00:00','2021-10-02 16:00:00'),(1,3,'2021-12-03 16:00:00','2021-12-03 16:00:00');
/*!40000 ALTER TABLE `pegawai_layanan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pengembangan`
--

DROP TABLE IF EXISTS `pengembangan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `pengembangan` (
  `pengembangan_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `layanan_id` int(11) NOT NULL,
  `klien_id` int(11) NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tiket_jenis_id` int(11) NOT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pengembangan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pengembangan`
--

LOCK TABLES `pengembangan` WRITE;
/*!40000 ALTER TABLE `pengembangan` DISABLE KEYS */;
/*!40000 ALTER TABLE `pengembangan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `replies`
--

DROP TABLE IF EXISTS `replies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `replies` (
  `replies_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reply` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`replies_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `replies`
--

LOCK TABLES `replies` WRITE;
/*!40000 ALTER TABLE `replies` DISABLE KEYS */;
/*!40000 ALTER TABLE `replies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `review` (
  `review_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `layanan_id` int(11) DEFAULT NULL,
  `klien_id` int(11) DEFAULT NULL,
  `tiket_id` int(11) NOT NULL,
  `pengembangan_id` int(11) DEFAULT NULL,
  `review` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`review_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (1,1,1,17,NULL,'like','2021-12-17 14:43:19','2021-12-17 14:43:19'),(2,1,1,18,NULL,'like','2021-12-21 12:54:09','2021-12-21 12:54:09'),(3,1,1,26,NULL,'dislike','2021-12-21 12:54:15','2021-12-21 12:54:15'),(4,1,1,19,NULL,'like','2021-12-21 12:56:02','2021-12-21 12:56:02'),(5,1,1,24,NULL,'dislike','2021-12-21 13:03:04','2021-12-21 13:03:04');
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `roles` (
  `roles_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`roles_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'user','2021-10-02 16:00:00','2021-10-02 16:00:00'),(2,'teknisi','2021-10-02 16:00:00','2021-10-02 16:00:00'),(3,'admin','2021-10-02 16:00:00','2021-10-02 16:00:00');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `selesai`
--

DROP TABLE IF EXISTS `selesai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `selesai` (
  `selesai_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pesan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lampiran` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`selesai_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `selesai`
--

LOCK TABLES `selesai` WRITE;
/*!40000 ALTER TABLE `selesai` DISABLE KEYS */;
/*!40000 ALTER TABLE `selesai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tagihan`
--

DROP TABLE IF EXISTS `tagihan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tagihan` (
  `invoice_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `due_date` date NOT NULL,
  `klien_id` int(11) NOT NULL,
  `tiket_id` int(11) NOT NULL,
  `pembayaran` int(11) NOT NULL,
  `sisa_pembayaran` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tagihan`
--

LOCK TABLES `tagihan` WRITE;
/*!40000 ALTER TABLE `tagihan` DISABLE KEYS */;
INSERT INTO `tagihan` VALUES (2,'2021-12-16','2021-12-23',1,19,100000,1000,'2021-12-16 12:36:57','2021-12-18 09:20:22'),(3,'2021-12-21','2021-12-28',1,24,200000,200000,'2021-12-21 00:54:02','2021-12-21 00:54:02'),(4,'2021-12-21','2021-12-28',1,22,1200000,1200000,'2021-12-21 13:02:15','2021-12-21 13:02:15');
/*!40000 ALTER TABLE `tagihan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiket`
--

DROP TABLE IF EXISTS `tiket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tiket` (
  `tiket_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `layanan_id` int(11) NOT NULL,
  `klien_id` int(11) NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tiket_jenis_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tiket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiket`
--

LOCK TABLES `tiket` WRITE;
/*!40000 ALTER TABLE `tiket` DISABLE KEYS */;
INSERT INTO `tiket` VALUES (17,1,1,'Wayan','eror',NULL,1,'2021-11-09 05:48:20','2021-12-09 05:48:20'),(18,1,1,'eror','eror',NULL,1,'2021-10-09 05:52:03','2021-12-09 05:52:03'),(19,1,1,'diproses','eror',NULL,1,'2021-12-09 05:59:12','2021-12-09 05:59:12'),(20,3,2,'eror','eror',NULL,1,'2021-12-09 21:52:39','2021-12-09 21:52:39'),(21,3,2,'eror','eror',NULL,1,'2021-12-09 21:52:40','2021-12-09 21:52:40'),(22,1,1,'Jadwal Pembelajaran Online Tahun Pelajaran 2020-2021','You can hide the location of your home, office or other private places in your activities.',NULL,1,'2021-12-14 20:35:57','2021-12-14 20:35:57'),(23,2,1,'Jadwal Pembelajaran Online Tahun Pelajaran 2020-2021','You can hide the location of your home, office or other private places in your activities.',NULL,1,'2021-12-14 20:47:03','2021-12-14 20:47:03'),(24,1,1,'Jadwal Pembelajaran Online Tahun Pelajaran 2020-2021','You can hide the location of your home, office or other private places in your activities.',NULL,2,'2021-12-14 20:48:21','2021-12-14 20:48:21'),(25,1,1,'Ebook Budidaya Strawberry Dalam Polybag Atau Pot 2','Subscribe to stay motivated with custom progress, segment and power goals.',NULL,1,'2021-12-14 22:05:36','2021-12-14 22:05:36'),(26,1,1,'Jadwal Pembelajaran Online Tahun Pelajaran 2020-2021','Jadwal Pembelajaran Online Tahun Pelajaran 2020-2021',NULL,1,'2021-12-21 01:56:53','2021-12-21 01:56:53'),(27,2,1,'Ebook Budidaya Tanaman Mint Dalam Polybag Atau Pot','Jadwal Pembelajaran Online Tahun Pelajaran 2020-2021',NULL,2,'2021-12-21 01:58:08','2021-12-21 01:58:08'),(28,1,1,'Ebook Budidaya Tanaman Mint Dalam Polybag Atau Pot','Ebook Budidaya Tanaman Mint Dalam Polybag Atau Pot',NULL,1,'2021-12-21 04:00:54','2021-12-21 04:00:54');
/*!40000 ALTER TABLE `tiket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiket_diproses`
--

DROP TABLE IF EXISTS `tiket_diproses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tiket_diproses` (
  `tiket_diproses_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `layanan_id` int(11) NOT NULL,
  `klien_id` int(11) NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tiket_diproses_id`),
  UNIQUE KEY `tiket_diproses_layanan_id_unique` (`layanan_id`),
  UNIQUE KEY `tiket_diproses_klien_id_unique` (`klien_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiket_diproses`
--

LOCK TABLES `tiket_diproses` WRITE;
/*!40000 ALTER TABLE `tiket_diproses` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiket_diproses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiket_foto`
--

DROP TABLE IF EXISTS `tiket_foto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tiket_foto` (
  `tiket_foto_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tiket_id` int(11) NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tiket_foto_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiket_foto`
--

LOCK TABLES `tiket_foto` WRITE;
/*!40000 ALTER TABLE `tiket_foto` DISABLE KEYS */;
INSERT INTO `tiket_foto` VALUES (3,8,'163730283515.jpg','2021-11-18 22:20:35','2021-11-18 22:20:35'),(4,10,'163747227815.jpg','2021-11-20 21:24:38','2021-11-20 21:24:38'),(5,11,'16378531434.jpg','2021-11-25 07:12:24','2021-11-25 07:12:24'),(6,12,'163819714513.jpg','2021-11-29 06:45:45','2021-11-29 06:45:45'),(7,13,'163896594311.png','2021-12-08 04:19:03','2021-12-08 04:19:03'),(8,14,'163897471311.jpg','2021-12-08 06:45:13','2021-12-08 06:45:13'),(10,16,'16390576831.jpg','2021-12-09 05:48:03','2021-12-09 05:48:03'),(11,17,'16390577002.jpg','2021-12-09 05:48:20','2021-12-09 05:48:20'),(12,18,'163905792315.jpg','2021-12-09 05:52:03','2021-12-09 05:52:03'),(13,19,'163905835210.jpg','2021-12-09 05:59:12','2021-12-09 05:59:12'),(14,20,'163911555911.png','2021-12-09 21:52:40','2021-12-09 21:52:40'),(15,21,'16391155602.png','2021-12-09 21:52:40','2021-12-09 21:52:40'),(16,22,'foto_pengaduan/jadwal-pembelajaran-online-tahun-pelajaran-2020-2021-1639542957.png',NULL,NULL),(17,23,'foto_pengaduan/jadwal-pembelajaran-online-tahun-pelajaran-2020-2021-1639543623.png','2021-12-14 20:47:03','2021-12-14 20:47:03'),(18,24,'foto_pengaduan/jadwal-pembelajaran-online-tahun-pelajaran-2020-2021-1639543701.png','2021-12-14 20:48:21','2021-12-14 20:48:21'),(19,25,'foto_pengaduan/ebook-budidaya-strawberry-dalam-polybag-atau-pot-2-1639548336.png','2021-12-14 22:05:36','2021-12-14 22:05:36'),(20,26,'foto_pengaduan/jadwal-pembelajaran-online-tahun-pelajaran-2020-2021-1640051813.png','2021-12-21 01:56:53','2021-12-21 01:56:53'),(21,27,'foto_pengaduan/ebook-budidaya-tanaman-mint-dalam-polybag-atau-pot-1640051888.png','2021-12-21 01:58:08','2021-12-21 01:58:08');
/*!40000 ALTER TABLE `tiket_foto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiket_jenis`
--

DROP TABLE IF EXISTS `tiket_jenis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tiket_jenis` (
  `tiket_jenis_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tiket_jenis_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiket_jenis`
--

LOCK TABLES `tiket_jenis` WRITE;
/*!40000 ALTER TABLE `tiket_jenis` DISABLE KEYS */;
INSERT INTO `tiket_jenis` VALUES (1,'Pengaduan','2021-10-02 16:00:00','2021-10-02 16:00:00'),(2,'Pengembangan','2021-10-02 16:00:00','2021-10-02 16:00:00');
/*!40000 ALTER TABLE `tiket_jenis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiket_log`
--

DROP TABLE IF EXISTS `tiket_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tiket_log` (
  `tiket_log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tiket_id` int(11) NOT NULL,
  `jenis_log` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tiket_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiket_log`
--

LOCK TABLES `tiket_log` WRITE;
/*!40000 ALTER TABLE `tiket_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiket_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiket_pegawai`
--

DROP TABLE IF EXISTS `tiket_pegawai`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tiket_pegawai` (
  `tiket_pegawai_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tiket_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tiket_pegawai_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiket_pegawai`
--

LOCK TABLES `tiket_pegawai` WRITE;
/*!40000 ALTER TABLE `tiket_pegawai` DISABLE KEYS */;
INSERT INTO `tiket_pegawai` VALUES (1,'teknisi1',16,'2021-12-09 05:48:03','2021-12-09 05:48:03'),(2,'teknisi1',17,'2021-12-09 05:48:20','2021-12-09 05:48:20'),(3,'teknisi1',18,'2021-12-09 05:52:03','2021-12-09 05:52:03'),(4,'teknisi1',19,'2021-12-09 05:59:12','2021-12-09 05:59:12'),(5,'teknisi1',20,'2021-12-09 21:52:40','2021-12-09 21:52:40'),(6,'teknisi1',21,'2021-12-09 21:52:40','2021-12-09 21:52:40'),(7,'teknisi1',22,NULL,'2021-12-21 12:58:39'),(8,'teknisi1',23,'2021-12-14 20:47:03','2021-12-14 20:47:03'),(9,'teknisi1',24,'2021-12-14 20:48:21','2021-12-21 00:54:08'),(10,'teknisi1',25,'2021-12-14 22:05:36','2021-12-14 22:05:36'),(11,'teknisi1',26,'2021-12-21 01:56:53','2021-12-21 04:07:33'),(12,'teknisi1',27,'2021-12-21 01:58:08','2021-12-21 03:59:51'),(13,'teknisi1',28,'2021-12-21 04:00:54','2021-12-21 04:03:07');
/*!40000 ALTER TABLE `tiket_pegawai` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiket_pesan`
--

DROP TABLE IF EXISTS `tiket_pesan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tiket_pesan` (
  `tiket_pesan_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tiket_id` int(11) NOT NULL,
  `pesan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tiket_pesan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiket_pesan`
--

LOCK TABLES `tiket_pesan` WRITE;
/*!40000 ALTER TABLE `tiket_pesan` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiket_pesan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiket_status`
--

DROP TABLE IF EXISTS `tiket_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tiket_status` (
  `tiket_status_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tiket_id` int(11) NOT NULL,
  PRIMARY KEY (`tiket_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiket_status`
--

LOCK TABLES `tiket_status` WRITE;
/*!40000 ALTER TABLE `tiket_status` DISABLE KEYS */;
INSERT INTO `tiket_status` VALUES (1,'menunggu','2021-11-18 22:20:35','2021-11-18 22:20:35',8),(2,'menunggu','2021-11-20 21:24:38','2021-11-20 21:24:38',10),(3,'menunggu','2021-11-25 07:12:24','2021-11-25 07:12:24',11),(4,'menunggu','2021-11-29 06:45:45','2021-11-29 06:45:45',12),(5,'menunggu','2021-12-08 04:19:03','2021-12-08 04:19:03',13),(6,'menunggu','2021-12-08 06:45:13','2021-12-08 06:45:13',14),(8,'menunggu','2021-12-09 05:48:03','2021-12-09 05:48:03',16),(9,'selesai','2021-12-09 05:48:20','2021-12-15 06:09:55',17),(10,'selesai','2021-12-09 05:52:03','2021-12-15 06:12:06',18),(11,'selesai','2021-12-09 05:59:12','2021-12-21 12:55:40',19),(12,'menunggu','2021-12-09 21:52:40','2021-12-09 21:52:40',20),(13,'menunggu','2021-12-09 21:52:40','2021-12-09 21:52:40',21),(14,'diproses',NULL,'2021-12-21 12:58:39',22),(15,'menunggu','2021-12-14 20:47:03','2021-12-14 20:47:03',23),(16,'selesai','2021-12-14 20:48:21','2021-12-21 00:56:13',24),(17,'menunggu','2021-12-14 22:05:36','2021-12-14 22:05:36',25),(18,'selesai','2021-12-21 01:56:53','2021-12-21 04:20:59',26),(19,'diproses','2021-12-21 01:58:08','2021-12-21 03:59:51',27),(20,'selesai','2021-12-21 04:00:54','2021-12-21 04:21:50',28);
/*!40000 ALTER TABLE `tiket_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiket_status_diproses`
--

DROP TABLE IF EXISTS `tiket_status_diproses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `tiket_status_diproses` (
  `tiket_status_diproses_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `progress` int(11) DEFAULT NULL,
  `tiket_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`tiket_status_diproses_id`),
  UNIQUE KEY `tiket_status_diproses_tiket_id_unique` (`tiket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiket_status_diproses`
--

LOCK TABLES `tiket_status_diproses` WRITE;
/*!40000 ALTER TABLE `tiket_status_diproses` DISABLE KEYS */;
INSERT INTO `tiket_status_diproses` VALUES (1,100,19,'2021-12-15 04:34:36','2021-12-21 12:55:40'),(2,100,17,'2021-12-15 05:16:55','2021-12-15 06:09:55'),(3,100,24,'2021-12-21 00:51:33','2021-12-21 00:56:13'),(4,NULL,27,'2021-12-21 03:59:51','2021-12-21 03:59:51'),(5,100,28,'2021-12-21 04:03:07','2021-12-21 04:21:50'),(6,100,26,'2021-12-21 04:07:33','2021-12-21 04:20:59'),(7,NULL,22,'2021-12-21 12:58:39','2021-12-21 12:58:39');
/*!40000 ALTER TABLE `tiket_status_diproses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `id` int(11) DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (13,13,'amik','user','$2y$10$ljlw4Q9FzGaDApTU2Z.0O.LebzVGY2K2R75vQTHNkyU0Qbhr75Iti',1,'','2021-10-19 22:20:36','2021-10-19 22:20:36'),(14,14,'ksusedana','user','$2y$10$ljlw4Q9FzGaDApTU2Z.0O.LebzVGY2K2R75vQTHNkyU0Qbhr75Iti',1,NULL,'2021-10-19 22:22:28','2021-10-19 22:22:28'),(15,15,'yoga','user','$2y$10$ljlw4Q9FzGaDApTU2Z.0O.LebzVGY2K2R75vQTHNkyU0Qbhr75Iti',1,NULL,'2021-10-19 22:23:42','2021-10-19 22:23:42'),(16,16,'buwayan','user','$2y$10$ljlw4Q9FzGaDApTU2Z.0O.LebzVGY2K2R75vQTHNkyU0Qbhr75Iti',1,NULL,'2021-10-19 22:24:31','2021-10-19 22:24:31'),(17,17,'buayu','user','$2y$10$ljlw4Q9FzGaDApTU2Z.0O.LebzVGY2K2R75vQTHNkyU0Qbhr75Iti',1,NULL,'2021-10-19 22:25:13','2021-10-19 22:25:13'),(18,18,'teknisi1','teknisi','$2y$10$ljlw4Q9FzGaDApTU2Z.0O.LebzVGY2K2R75vQTHNkyU0Qbhr75Iti',2,'','2021-10-19 22:26:32','2021-10-19 22:26:32'),(19,19,'teknisi2','teknisi','$2y$10$ljlw4Q9FzGaDApTU2Z.0O.LebzVGY2K2R75vQTHNkyU0Qbhr75Iti',2,NULL,'2021-11-29 07:25:46','2021-11-29 07:25:46');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-21 21:35:09
