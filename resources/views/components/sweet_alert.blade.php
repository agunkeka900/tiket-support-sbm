<link rel="stylesheet" href="{{ url('plugins/sweetalert/dist/sweetalert.css') }}">
<script src="{{ url('plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
<script>
  @if(Session::has('success'))
    !function($) {
      "use strict";
      var SweetAlert = function() {};

      SweetAlert.prototype.init = function() {
        swal({
          title: 'Berhasil',
          text: "{{ Session::get('success') }}",
          type: 'success',
          confirmButtonColor: '#188AE2',
          timer: 1500
        })
      },
      $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
    }(window.jQuery),

    function($) {
      "use strict";
      $.SweetAlert.init()
    }(window.jQuery);
  @elseif(Session::has('success_stay'))
    !function($) {
      "use strict";
      var SweetAlert = function() {};

      SweetAlert.prototype.init = function() {
        swal({
          title: 'Berhasil',
          text: "{{ Session::get('success_stay') }}",
          type: 'success',
          confirmButtonColor: '#188AE2'
        })
      },
      $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
    }(window.jQuery),

    function($) {
      "use strict";
      $.SweetAlert.init()
    }(window.jQuery);
  @elseif(count($errors) > 0)
    @php
      $error_msg = [];
      foreach($errors->all() as $error){
        $error_msg[] = $error;
      }
    @endphp

    !function($) {
      "use strict";
      var SweetAlert = function() {};

      SweetAlert.prototype.init = function() {
        swal("Gagal", "{{ implode('\n', $error_msg) }}", "error")
      },
      $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
    }(window.jQuery),

    function($) {
      "use strict";
      $.SweetAlert.init()
    }(window.jQuery);
  @endif
</script>