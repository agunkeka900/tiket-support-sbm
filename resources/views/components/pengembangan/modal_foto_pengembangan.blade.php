<div id="modal{{ $d->tiket_id }}" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Foto Pengaduan</h4>
      </div>
      <div class="modal-body text-center">
        @if($d->url)
          <img src="{{ url($d->url) }}" alt="Foto" width="100%">
        @endif
      </div>
    </div>
  </div>
</div>