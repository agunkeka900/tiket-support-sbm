<div id="modal-tagihan{{ $d->tiket_id }}" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Input Tagihan</h4>
      </div>
      <form action="{{ url('tagihan') }}" method="post">
        @csrf
        <input type="hidden" name="tiket_id" value="{{ $d->tiket_id }}">
        <input type="hidden" name="klien_id" value="{{ $d->klien_id }}">
        <div class="modal-body">
          <div class="form-group">
            <label for="judul">Judul</label>
            <input type="text" id="judul" readonly value="{{ $d->judul }}" class="form-control">
          </div>
          <div class="form-group">
            <label for="klien">Klien</label>
            <input type="text" id="klien" readonly value="{{ $d->nama_klien }}" class="form-control">
          </div>
          <div class="form-group">
            <label for="date">Date</label>
            <input type="date" id="date" name="date" value="{{ date('Y-m-d') }}" class="form-control">
          </div>
          <div class="form-group">
            <label for="due_date">Due Date</label>
            <input type="date" id="due_date" name="due_date" value="{{ date('Y-m-d', strtotime(date('Y-m-d')) + (86400*7)) }}" class="form-control">
          </div>
          <div class="form-group">
            <label for="pembayaran">Jumlah Tagihan</label>
            <input type="number" id="pembayaran" name="pembayaran" class="form-control" min="1" required>
          </div>
        </div>
        <div class="modal-footer text-right">
          <button class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>