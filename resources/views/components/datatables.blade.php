<link rel="stylesheet" href="{{ asset('bower_components/datatables.net/css/datatables.css') }}">
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(function () {
        $('#datatable').DataTable();
    })
</script>