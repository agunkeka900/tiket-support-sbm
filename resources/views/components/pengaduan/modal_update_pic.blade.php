<div id="modal-pic{{ $d->tiket_id }}" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Update PiC (Personal in Charge)</h4>
      </div>
      <form action="{{ url('pengaduan/pic') }}" method="post">
        @csrf @method('put')
        <input type="hidden" name="tiket_id" value="{{ $d->tiket_id }}">
        <div class="modal-body">
          <div class="form-group">
            <label for="judul">Judul</label>
            <input type="text" id="judul" readonly value="{{ $d->judul }}" class="form-control">
          </div>
          <div class="form-group">
            <label for="pic">PiC</label>
            <input type="text" id="pic" readonly value="{{ Auth::user()->username }}" class="form-control">
          </div>
          <div class="form-group">
            <label for="status">Status</label>
            <select name="status" id="status" class="form-control">
              @foreach($data_status as $s)
                <option value="{{ $s }}" {{ $s == $d->status ? 'selected' : '' }}>{{ ucfirst($s) }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="modal-footer text-right">
          <button class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>