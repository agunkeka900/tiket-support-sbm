<div id="modal-up{{ $d->tiket_id }}" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title">Update Progress</h4>
      </div>
      <form action="{{ url('pengaduan/update-progress') }}" method="post" enctype="multipart/form-data">
        @csrf @method('put')
        <input type="hidden" name="tiket_id" value="{{ $d->tiket_id }}">
        <div class="modal-body">
          <div class="form-group">
            <label for="judul">Judul</label>
            <input type="text" id="judul" readonly value="{{ $d->judul }}" class="form-control">
          </div>
          <div class="form-group">
            <label for="progress">Progress (%)</label>
            <input type="number" min="1" max="100" name="progress" id="progress" value="{{ $d->progress }}" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="keterangan">Keterangan</label>
            <textarea name="keterangan" id="keterangan" class="form-control" rows="5"></textarea>
          </div>
          <div class="form-group">
            <label for="keterangan">Foto</label>
            <input type="file"
                   name="foto"
                   class="dropify dropify-photo"
                   data-height="150"
                   data-show-remove="false"
                   data-allowed-file-extensions="png jpg jpeg"
                   data-max-file-size="1M"
                   accept="image/jpeg, image/jpg, image/png" />
          </div>
        </div>
        <div class="modal-footer text-right">
          <button onclick="document.getElementById('form-selesai{{ $d->tiket_id }}').submit()"
                  type="button" class="btn btn-default">Update Status Pengaduan Menjadi Selesai</button>
          <button class="btn btn-primary">Simpan</button>
        </div>
      </form>
      <form action="{{ url('pengaduan/selesai-progress') }}" method="post" id="form-selesai{{ $d->tiket_id }}">
        @csrf @method('put')
        <input type="hidden" name="tiket_id" value="{{ $d->tiket_id }}">
      </form>
    </div>
  </div>
</div>