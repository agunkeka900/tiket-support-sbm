@extends('layouts.pegawai')

@section('title')
  Pegawai | Notifikasi
@endsection

@section('css')
@endsection

@section('content')
  <section class="content-header ml-0 pb-4 pt-5 px-5">
    <h1>
      <a href="{{ url('pegawai') }}">
        <i class="fa fa-arrow-left mr-3 text-primary"></i>
      </a>
      Notifikasi
    </h1>
    <ol class="breadcrumb mt-3 pt-3 mr-3">
      <li><a href="{{ url('pegawai') }}">Home</a></li>
      <li class="active">Notifikasi</li>
    </ol>
  </section>
  <section class="container-fluid px-5">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-primary mt-3">
          <div class="box-body px-4">
            <div class="row px-2">
              <div class="col-sm-12">
                <div class="table-responsive mailbox-messages">
                  <table class="table table-hover table-striped">
                    <tbody>
                    @foreach($data as $no=>$d)
                      <tr>
                        <td class="mailbox-name" style="min-width: 160px"><a href="{{ url('notifikasi/read/'.$d->notif_id.'?redirect_link='.$d->redirect_link) }}">{{ $d['judul'] }}</a></td>
                        <td class="mailbox-subject">{{ $d['keterangan'] }}</td>
                        <td class="mailbox-date" style="min-width: 180px"><i class="fa fa-clock-o"></i> {{ \App\Http\Controllers\HelperController::setNamaWaktu($d['created_at']) }}</td>
                        <td class="p-0">
                          <form action="{{ url('notifikasi') }}" method="post">
                            @csrf @method('delete')
                            <input type="hidden" name="notif_id" value="{{ $d->notif_id }}">
                            <input type="hidden" name="user_id" value="{{ $d->user_id }}">
                            <button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                          </form>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <style>
    .table tr td{
      font-size: 12px;
      vertical-align: top;
    }
  </style>
@endsection
@section('script')
  @include('components.sweet_alert')
  @include('components.datatables')
@endsection