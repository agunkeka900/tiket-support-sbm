@extends('layouts.klien')

@section('title')
  Klien | Menunggu
@endsection

@section('css')
@endsection

@section('content')
  <div class="content-wrapper ml-0">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <section class="content-header ml-0 py-5">
            <h1>
              <a href="{{ url('klien') }}">
                <i class="fa fa-arrow-left mr-3 text-primary"></i>
              </a>
              Data {{ $nama_jenis }} {{ $nama_layanan }} (Status Menunggu)
            </h1>
            <ol class="breadcrumb pt-4 mt-2">
              <li><a href="{{ url('klien') }}">Home</a></li>
              <li class="active">Data Pengaduan</li>
            </ol>
          </section>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="box box-primary mt-0">
            {{--<div class="box-header with-border">--}}

            {{--</div>--}}
            <div class="box-body px-4">
              <div class="row px-2">
                <div class="col-sm-12 px-5">
                  <table id="datatable" class="table table-sm table-bordered">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>ID Tiket</th>
                    <th>Judul</th>
                    <th>Deskripsi</th>
                    <th>Foto</th>
                    <th>Jenis</th>
                    <th>Status</th>
                    <th>Pegawai</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data as $no=>$d)
                    <tr>
                      <td class="text-center">{{ $no+1 }}</td>
                      <td>{{ $d['tiket_id'] }}</td>
                      <td>{{ $d['judul'] }}</td>
                      <td>{{ $d['deskripsi'] }}</td>
                      <td class="p-0 text-center">
                        @if($d['url'] != null)
                          <img src="{{ url($d['url']) }}" alt="Foto" height="50px" data-toggle="modal" data-target="#modal{{ $d->tiket_id }}" style="cursor: pointer">
                        @endif
                      </td>
                      <td>{{ $d['tiket_jenis'] }}</td>
                      <td>{{ $d['status'] }}</td>
                      <td>{{ $d['pegawai'] }}</td>
                    </tr>
                  @endforeach
                  </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @foreach($data as $d)
    <div id="modal{{ $d->tiket_id }}" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Foto Pengaduan</h4>
          </div>
        <div class="modal-body text-center">
          <img src="{{ url($d->url) }}" alt="Foto" width="100%">
        </div>
        </div>
      </div>
    </div>
  @endforeach
@endsection
@section('script')
  @include('components.sweet_alert')
  @include('components.datatables')
@endsection