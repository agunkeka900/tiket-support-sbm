@extends('layouts.pegawai')

@section('title')
  Pegawai | Pengaduan
@endsection

@section('css')
@endsection

@section('content')
  <section class="content-header ml-0 py-3 px-5">
    <h1>
      Pengaduan
    </h1>
    <ol class="breadcrumb mt-3 pt-3 mr-3">
      <li><a href="{{ url('pegawai') }}">Home</a></li>
      <li class="active">Pengaduan</li>
    </ol>
  </section>
  <section class="container-fluid px-5">
    <div class="row">
      <div class="col-sm-12">
        @foreach($layanan as $l)
          <div class="box box-solid box-primary mt-3">
            <div class="box-header with-border">
              <div class="row px-4">
                <div class="col-sm-6">
                  <h3 class="box-title mt-1">{{ $l['nama'] }}</h3>
                </div>
              </div>
            </div>
            <div class="box-body px-4">
              <div class="row px-2">
                <div class="col-sm-4 px-2">
                  <a href="{{ url('pegawai/pengaduan/menunggu/'.$l->layanan_id) }}" class="info-box bg-green mb-0">
                    <span class="info-box-icon"><i class="fa fa-hourglass-start"></i></span>
                    <div class="info-box-content pt-4">
                      <span class="info-box-text">Menunggu</span>
                      <span class="info-box-number" style="font-size: 22pt">{{ $l['menunggu'] }}</span>
                    </div>
                  </a>
                </div>
                <div class="col-sm-4 px-2">
                  <a href="{{ url('pegawai/pengaduan/diproses/'.$l->layanan_id) }}" class="info-box bg-yellow mb-0">
                    <span class="info-box-icon"><i class="fa fa-wrench"></i></span>
                    <div class="info-box-content pt-4">
                      <span class="info-box-text">Diproses</span>
                      <span class="info-box-number" style="font-size: 22pt">{{ $l['diproses'] }}</span>
                    </div>
                  </a>
                </div>
                <div class="col-sm-4 px-2">
                  <a  href="{{ url('pegawai/pengaduan/selesai/'.$l->layanan_id) }}"class="info-box bg-red mb-0">
                    <span class="info-box-icon"><i class="fa fa-check"></i></span>
                    <div class="info-box-content pt-4">
                      <span class="info-box-text">Selesai</span>
                      <span class="info-box-number" style="font-size: 22pt">{{ $l['selesai'] }}</span>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
@endsection
@section('script')
  @include('components.sweet_alert')
@endsection