@extends('layouts.pegawai')

@section('title')
  Pegawai | Selesai
@endsection

@section('css')
@endsection

@section('content')
  <section class="content-header ml-0 pb-4 pt-5 px-5">
    <h1>
      <a href="{{ url('pegawai/pengaduan') }}">
        <i class="fa fa-arrow-left mr-3 text-primary"></i>
      </a>
      Pengaduan {{ $nama_layanan }} (Selesai)
    </h1>
    <ol class="breadcrumb mt-3 pt-3 mr-3">
      <li><a href="{{ url('pegawai') }}">Home</a></li>
      <li><a href="{{ url('pegawai/pengaduan') }}">Pengaduan</a></li>
      <li class="active">Selesai</li>
    </ol>
  </section>
  <section class="container-fluid px-5">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-primary mt-3">
          <div class="box-body px-4">
            <div class="row px-2">
              <div class="col-sm-12 px-5 pt-3">
                <table id="datatable" class="table table-sm table-bordered">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>ID Tiket</th>
                      <th>Judul</th>
                      <th>Deskripsi</th>
                      <th>Foto</th>
                      <th>Pegawai</th>
                      <th style="width: 60px">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($data as $no=>$d)
                    <tr>
                      <td class="text-center">{{ $no+1 }}</td>
                      <td>{{ $d['tiket_id'] }}</td>
                      <td>{{ $d['judul'] }}</td>
                      <td>{{ $d['deskripsi'] }}</td>
                      <td class="p-0 text-center">
                        @if($d['url'] != null)
                          <img src="{{ url($d['url']) }}" alt="Foto" height="50px" data-toggle="modal" data-target="#modal{{ $d->tiket_id }}" style="cursor: pointer">
                        @endif
                      </td>
                      <td>{{ $d['pegawai'] }}</td>
                      <td class="p-1 text-center">
                        <div class="btn-group">
                          <a href="{{ url('pegawai/pengaduan/selesai/detail/'.$d['tiket_id']) }}" class="btn btn-primary">Detail</a>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @foreach($data as $d)
    @include('components.pengaduan.modal_foto_pengaduan')
  @endforeach
@endsection
@section('script')
  @include('components.sweet_alert')
  @include('components.datatables')
@endsection