@extends('layouts.pegawai')

@section('title')
  Pegawai | Menunggu
@endsection

@section('css')
  <link rel="stylesheet" href="{{ url('plugins/dropify/css/dropify.css') }}">
@endsection

@section('content')
  <section class="content-header ml-0 pb-4 pt-5 px-5">
    <h1>
      <a href="{{ url('pegawai/pengaduan') }}">
        <i class="fa fa-arrow-left mr-3 text-primary"></i>
      </a>
      Pengaduan {{ $nama_layanan }} (Diproses)
    </h1>
    <ol class="breadcrumb mt-3 pt-3 mr-3">
      <li><a href="{{ url('pegawai') }}">Home</a></li>
      <li><a href="{{ url('pegawai/pengaduan') }}">Pengaduan</a></li>
      <li class="active">Diproses</li>
    </ol>
  </section>
  <section class="container-fluid px-5">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-primary mt-3">
          <div class="box-body px-4">
            <div class="row px-2">
              <div class="col-sm-12 px-5 pt-3">
                <table id="datatable" class="table table-sm table-bordered">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>ID Tiket</th>
                      <th>Judul</th>
                      <th>Deskripsi</th>
                      <th>Foto</th>
                      {{--<th>Jenis</th>--}}
                      <th>Progress</th>
                      <th>Pegawai</th>
                      <th style="width: 60px">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($data as $no=>$d)
                    <tr>
                      <td class="text-center">{{ $no+1 }}</td>
                      <td>{{ $d['tiket_id'] }}</td>
                      <td>{{ $d['judul'] }}</td>
                      <td>{{ $d['deskripsi'] }}</td>
                      <td class="p-0 text-center">
                        @if($d['url'] != null)
                          <img src="{{ url($d['url']) }}" alt="Foto" height="50px" data-toggle="modal" data-target="#modal{{ $d->tiket_id }}" style="cursor: pointer">
                        @endif
                      </td>
                      {{--<td>{{ $d['tiket_jenis'] }}</td>--}}
                      <td class="py-1 text-right">
                        @php
                          $progress = $d['progress'] != null ? $d['progress'] : 0;
                        @endphp
                        {{--@if($d['progress'] >= 1)--}}
                          <div class="progress progress-sm active">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                                 aria-valuenow="{{ $progress }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $progress }}%">
                              <span class="sr-only">{{ $progress }}%</span>
                            </div>
                          </div>
                          {{  $progress }}%
                        {{--@endif--}}
                      </td>
                      <td>{{ $d['pegawai'] }}</td>
                      <td class="p-1 text-center">
                        <div class="btn-group">
                          <a href="{{ url('pegawai/pengaduan/diproses/detail/'.$d['tiket_id']) }}" class="btn btn-primary">Detail</a>
                          <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span class="caret"></span>
                          </button>
                          <ul class="dropdown-menu" style="min-width: 0">
                            @if($d['pegawai'] != Auth::user()->username)
                              <li><a href="#" class="px-3" data-toggle="modal" data-target="#modal-pic{{ $d->tiket_id }}">Update PiC</a></li>
                            @endif
                            <li><a href="#" class="px-3" data-toggle="modal" data-target="#modal-up{{ $d->tiket_id }}">Update Progress</a></li>
                            <li><a href="#" class="px-3" data-toggle="modal" data-target="#modal-tagihan{{ $d->tiket_id }}">Input Tagihan</a></li>
                          </ul>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @foreach($data as $d)
    @include('components.pengaduan.modal_foto_pengaduan')
    @include('components.pengaduan.modal_update_pic')
    @include('components.pengaduan.modal_update_progress')
    @include('components.pengaduan.modal_tagihan')
  @endforeach
@endsection
@section('script')
  @include('components.sweet_alert')
  @include('components.datatables')
  <script src="{{ url('plugins/dropify/js/dropify.min.js') }}"></script>
  <script>
      $(document).ready(function(){
          $('.dropify-photo').dropify({
              messages: {
                  'default': 'Drag & drop gambar di sini atau klik',
                  'replace': 'Drag & drop atau klik untuk mengganti gambar',
                  'error': 'Hanya format png, jpg, jpeg yang di izinkan'
              },
              error: {
                  'fileSize': 'Ukuran gambar melebihi maksimal (1M max).',
              }
          });
      });
  </script>
@endsection