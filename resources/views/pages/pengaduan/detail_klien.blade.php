@extends('layouts.klien')

@section('title')
  Klien | Pengaduan Detail
@endsection

@section('css')
  <link rel="stylesheet" href="{{ url('plugins/dropify/css/dropify.css') }}">
@endsection

@section('content')

  <div class="content-wrapper ml-0">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <section class="content-header ml-0 py-5">
            <h1>
              <a href="{{ url('klien/'.$back_url.'/'.$data_tiket['tiket_jenis_id'].'/'.$data_tiket['layanan_id']) }}">
                <i class="fa fa-arrow-left mr-3 text-primary"></i>
              </a>
              Pengaduan Detail
            </h1>
            <ol class="breadcrumb pt-4 mt-2">
              <li><a href="{{ url('klien') }}">Home</a></li>
              <li><a href="{{ url('klien/'.$back_url.'/'.$data_tiket['tiket_jenis_id'].'/'.$data_tiket['layanan_id']) }}">{{ ucwords($back_url) }}</a></li>
              <li class="active">Detail</li>
            </ol>
          </section>
        </div>
      </div>
    <div class="row">
      <div class="col-sm-6 px-2">
        <div class="box box-primary mt-3">
          <div class="box-body px-4">
            <table class="table">
              <tr>
                <td style="width: 150px">ID Tiket</td>
                <td style="width: 5px">:</td>
                <td>{{ $data_tiket['tiket_id'] }}</td>
              </tr>
              <tr>
                <td style="width: 150px">Nama Layanan</td>
                <td style="width: 5px">:</td>
                <td>{{ $data_tiket['nama_layanan'] }}</td>
              </tr>
              <tr>
                <td>Nama Klien</td>
                <td>:</td>
                <td>{{ $data_tiket['nama_klien'] }}</td>
              </tr>
              <tr>
                <td>Judul Pengaduan</td>
                <td>:</td>
                <td>{{ $data_tiket['judul'] }}</td>
              </tr>
              <tr>
                <td>Deskripsi Pengaduan</td>
                <td>:</td>
                <td>{{ $data_tiket['deskripsi'] }}</td>
              </tr>
              <tr>
                <td>Foto Pengaduan</td>
                <td>:</td>
                <td>
                  @if($data_tiket['url'] != null)
                    <img src="{{ url($data_tiket['url']) }}" alt="Foto" style="width: 100%">
                  @endif
                </td>
              </tr>
              <tr>
                <td>Progress Terakhir</td>
                <td>:</td>
                <td class="text-right">
                  @php
                    $progress = $data_tiket['progress'] != null ? $data_tiket['progress'] : 0;
                  @endphp
                  <div class="progress progress-sm active">
                    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar"
                         aria-valuenow="{{ $progress }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $progress }}%">
                      <span class="sr-only">{{ $progress }}%</span>
                    </div>
                  </div>
                  {{  $progress }}%
                  @if($data_tiket['foto_progress'] != null)
                    <img src="{{ url($data_tiket['foto_progress']) }}" alt="Progress" style="width: 100%">
                  @endif
                  <p class="text-left">{{ $data_tiket['keterangan_progress'] }}</p>
                  <p class="text-muted">{{ \App\Http\Controllers\HelperController::setNamaWaktu($data_tiket['waktu_progress']) }}</p>
                </td>
              </tr>
              <tr>
                <td>Tiket Jenis</td>
                <td>:</td>
                <td>{{ $data_tiket['tiket_jenis'] }}</td>
              </tr>
              <tr>
                <td>Status</td>
                <td>:</td>
                <td>{{ $data_tiket['status'] }}</td>
              </tr>
              <tr>
                <td>Pegawai</td>
                <td>:</td>
                <td>{{ $data_tiket['pegawai'] }}</td>
              </tr>
              <tr>
                <td>Diajukan Pada</td>
                <td>:</td>
                <td>{{ \App\Http\Controllers\HelperController::setNamaWaktu($data_tiket['created_at']) }}</td>
              </tr>
            </table>
          </div>
        </div>
        @if(count($data_history_pic) >= 2)
          <div class="box box-success mt-3">
            <div class="box-header with-border">
              <h3 class="box-title">History PIC</h3>
            </div>
            <div class="box-body px-4">
              <table class="table table-sm table-bordered">
                <thead>
                <tr>
                  <th class="text-center">No</th>
                  <th>Pegawai</th>
                  <th>Tanggal</th>
                </tr>
                </thead>
                <tbody>
                @foreach($data_history_pic as $no=>$d)
                  <tr>
                    <td class="text-center">{{ $no+1 }}</td>
                    <td>{{ $d['nama'] }}</td>
                    <td>{{ \App\Http\Controllers\HelperController::setNamaWaktu($d['created_at']) }}</td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          </div>
        @endif
        @if($data_tagihan)
          <div class="box box-success mt-3">
            <div class="box-header with-border">
              <h3 class="box-title">Tagihan</h3>
            </div>
            <div class="box-body px-4">
              <table class="table">
                <tr>
                  <td style="width: 150px">Invoice ID</td>
                  <td style="width: 5px">:</td>
                  <td>{{ $data_tagihan['invoice_id'] }}</td>
                </tr>
                <tr>
                  <td>Nama Klien</td>
                  <td>:</td>
                  <td>{{ $data_tagihan['nama_klien'] }}</td>
                </tr>
                <tr>
                  <td>Jumlah Pembayaran</td>
                  <td>:</td>
                  <td>Rp {{ number_format($data_tagihan['pembayaran'], 0, ',', '.') }}</td>
                </tr>
                <tr>
                  <td>Sisa Pembayaran</td>
                  <td>:</td>
                  <td>Rp {{ number_format($data_tagihan['sisa_pembayaran'], 0, ',', '.') }}</td>
                </tr>
                <tr>
                  <td>Date</td>
                  <td>:</td>
                  <td>{{ \App\Http\Controllers\HelperController::setNamaBulan(null, $data_tagihan['date']) }}</td>
                </tr>
                <tr>
                  <td>Due Date</td>
                  <td>:</td>
                  <td>{{ \App\Http\Controllers\HelperController::setNamaBulan(null, $data_tagihan['due_date']) }}</td>
                </tr>
              </table>
            </div>
          </div>
        @endif
        @if($data_tiket['status'] == \App\TiketStatus::S_SELESAI)
          <div class="box box-success mt-3">
            <div class="box-header with-border">
              <h3 class="box-title">Review</h3>
            </div>
            <div class="box-body px-4 text-center">
              @if(isset($data_review))
                @if($data_review['review'] == \App\Review::LIKE)
                  <i class="fa fa-thumbs-up fa-5x"></i> <br><br>
                  Anda memberikan review "{{ $data_review['review'] }}" pada pengaduan ini
                @else
                  <i class="fa fa-thumbs-down fa-5x"></i> <br><br>
                  Anda memberikan review "{{ $data_review['review'] }}" pada pengaduan ini
                @endif
              @else
                <table style="width: 100%">
                  <tr>
                    <td colspan="4" class="pb-3">
                      Silahkan berikan review pada pengaduan ini
                    </td>
                  </tr>
                  <tr>
                    <td></td>
                    <td style="width: 120px; max-width: 120px; cursor: pointer" class="text-primary"
                        onclick="document.getElementById('form-like').submit()">
                      <form action="{{ url('review/like') }}" method="post" id="form-like">
                        @csrf
                        <input type="hidden" name="tiket_id" value="{{ $data_tiket['tiket_id'] }}">
                        <input type="hidden" name="layanan_id" value="{{ $data_tiket['layanan_id'] }}">
                        <input type="hidden" name="klien_id" value="{{ $data_tiket['klien_id'] }}">
                        <i class="fa fa-thumbs-up fa-5x"></i>
                        <br>
                        <br>
                        Like
                      </form>
                    </td>
                    <td style="width: 120px; max-width: 120px; cursor: pointer" class="text-primary"
                        onclick="document.getElementById('form-dislike').submit()">
                      <form action="{{ url('review/dislike') }}" method="post" id="form-dislike">
                        @csrf
                        <input type="hidden" name="tiket_id" value="{{ $data_tiket['tiket_id'] }}">
                        <input type="hidden" name="layanan_id" value="{{ $data_tiket['layanan_id'] }}">
                        <input type="hidden" name="klien_id" value="{{ $data_tiket['klien_id'] }}">
                        <i class="fa fa-thumbs-down fa-5x"></i>
                        <br>
                        <br>
                        Dislike
                      </form>
                    </td>
                    <td></td>
                  </tr>
                </table>
              @endif
            </div>
          </div>
        @endisset
      </div>
      <div class="col-sm-6 px-2">
        <div class="box box-primary mt-3">
          <div class="box-body px-4">
            <div class="box box-widget mt-2">
              <div class="box-footer box-comments">

                @foreach($data_comment as $c)
                  <div class="box-comment">
                    <img class="img-circle img-sm" src="{{ url('img/'.$c['foto']) }}" alt="User Image">
                    <div class="comment-text">
                      <span class="username">
                        {{ $c['nama'] }} <span class="badge" style="background-color: #337ab7; font-size: 10px">{{ $c['level'] }}</span>
                        <span class="text-muted pull-right">{{ $c['waktu'] }}</span>
                      </span>
                      {{ $c['comment'] }}
                      @if($c['lampiran'] != '-')
                        <br>
                        <div class="pt-2">
                          @if(\App\Http\Controllers\HelperController::isImage($c['lampiran']))
                            <img src="{{ url($c['lampiran']) }}" alt="Foto" style="width: 100% !important; height: 100% !important;">
                          @elseif(\App\Http\Controllers\HelperController::isPdf($c['lampiran']))
                            <iframe src="{{ url($c['lampiran']) }}" height="400" width="100%"></iframe>
                          @else
                            <a href="{{ url($c['lampiran']) }}" class="btn btn-success btn-sm">Download Lampiran</a>
                          @endif
                        </div>
                      @endif
                    </div>
                  </div>
                @endforeach
              </div>
              <div class="box-footer">
                <form action="{{ url('comment') }}" method="post" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="tiket_id" value="{{ $data_tiket['tiket_id'] }}">
                  <div class="img-push">
                    <input type="text" name="comment" class="form-control input-sm" placeholder="Masukkan komentar" required>
                  </div>
                  <div class="img-push pt-3" id="container-gambar" style="display: none;">
                    <input type="file"
                           name="file"
                           class="dropify dropify-photo"
                           data-height="150"
                           data-show-remove="false"
                           data-max-file-size="5M" />
                  </div>
                  <div class="img-push text-right">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" onchange="showGambar(this.checked)"> Masukkan Lampiran (Gambar/Dokumen)
                      </label>
                      <input type="submit" class="btn btn-primary ml-3" value="Kirim">
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
@endsection
@section('script')
  @include('components.sweet_alert')
  <script src="{{ url('plugins/dropify/js/dropify.min.js') }}"></script>
  <script>
      $(document).ready(function(){
          $('.dropify-photo').dropify({
              messages: {
                  'default': 'Drag & drop di sini atau klik',
                  'replace': 'Drag & drop atau klik untuk mengganti',
                  // 'error': 'Hanya format png, jpg, jpeg yang di izinkan'
              },
              error: {
                  'fileSize': 'Ukuran file melebihi maksimal (5M max).',
              }
          });
      });
      function showGambar(val) {
          if(val) $("#container-gambar").show();
          else $("#container-gambar").hide();
      }
  </script>
@endsection