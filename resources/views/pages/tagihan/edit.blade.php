@extends('layouts.pegawai')

@section('title')
  Pegawai | Edit Tagihan
@endsection

@section('css')
@endsection

@section('content')
  <section class="content-header ml-0 pb-4 pt-3 px-5">
    <h1>
      <a href="{{ url('pegawai/tagihan') }}">
        <i class="fa fa-arrow-left mr-3 text-primary"></i>
      </a>
      Edit Tagihan
    </h1>
    <ol class="breadcrumb mt-3 pt-3 mr-3">
      <li><a href="{{ url('pegawai') }}">Home</a></li>
      <li><a href="{{ url('pegawai/tagihan') }}">Data Tagihan</a></li>
      <li class="active">Edit</li>
    </ol>
  </section>
  <section class="container-fluid px-5">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-primary mt-3">
          <div class="box-body px-4">
            <div class="row px-2">
              <div class="col-sm-12 px-5 pt-3">
                <form action="{{ url('tagihan') }}" method="post">
                  @csrf @method('put')
                  <input type="hidden" name="invoice_id" value="{{ $data['invoice_id'] }}">
                  <div class="form-group row">
                    <label class="col-sm-3 control-label mt-2 text-right" for="pembayaran">Jumlah Pembayaran</label>
                    <div class="col-sm-6 pl-3">
                      <input type="number" class="form-control" id="pembayaran" name="pembayaran" value="{{ $data['pembayaran'] }}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 control-label mt-2 text-right" for="sisa_pembayaran">Sisa Pembayaran</label>
                    <div class="col-sm-6 pl-3">
                      <input type="number" class="form-control" id="sisa_pembayaran" name="sisa_pembayaran" value="{{ $data['sisa_pembayaran'] }}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 control-label mt-2 text-right" for="date">Date</label>
                    <div class="col-sm-6 pl-3">
                      <input type="date" class="form-control" id="date" name="date" value="{{ $data['date'] }}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-3 control-label mt-2 text-right" for="due_date">Due Date</label>
                    <div class="col-sm-6 pl-3">
                      <input type="date" class="form-control" id="due_date" name="due_date" value="{{ $data['due_date'] }}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-9 pl-3 text-right">
                      <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('script')
  @include('components.sweet_alert')
@endsection