@extends('layouts.pegawai')

@section('title')
  Pegawai | Tagihan
@endsection

@section('css')
@endsection

@section('content')
  <section class="content-header ml-0 pb-4 pt-5 px-5">
    <h1>
      Data Tagihan
    </h1>
    <ol class="breadcrumb mt-3 pt-3 mr-3">
      <li><a href="{{ url('pegawai') }}">Home</a></li>
      <li class="active">Data Tagihan</li>
    </ol>
  </section>
  <section class="container-fluid px-5">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-primary mt-3">
          <div class="box-body px-4">
            <div class="row px-2">
              <div class="col-sm-12 px-5 pt-3">
                <table id="datatable" class="table table-sm table-bordered">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Klien</th>
                    <th>Pegawai</th>
                    <th>Layanan</th>
                    <th>Jenis</th>
                    <th>Date</th>
                    <th>Due Date</th>
                    <th>Jumlah</th>
                    <th>Sisa</th>
                    <th style="width: 120px">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data as $no=>$d)
                    <tr>
                      <td class="text-center">{{ $no+1 }}</td>
                      <td>{{ $d['nama_klien'] }}</td>
                      <td>{{ $d['pegawai'] }}</td>
                      <td>{{ $d['nama_layanan'] }}</td>
                      <td>{{ $d['tiket_jenis'] }}</td>
                      <td>{{ \App\Http\Controllers\HelperController::setNamaBulan(null, $d['date']) }}</td>
                      <td>{{ \App\Http\Controllers\HelperController::setNamaBulan(null, $d['due_date']) }}</td>
                      <td class="text-right">{{ number_format($d['pembayaran'], 0, ',', '.') }}</td>
                      <td class="text-right">{{ number_format($d['sisa_pembayaran'], 0, ',', '.') }}</td>
                      <td class="p-1 text-center">
                        <div class="btn-group">
                          <a href="{{ url('pegawai/tagihan/edit/'.$d['tiket_id']) }}" class="btn btn-sm btn-primary">Edit</a>
                          <a href="{{ url('pegawai/pengaduan/'.$d['status'].'/detail/'.$d['tiket_id'].'?back_url=pegawai/tagihan') }}"
                             class="btn btn-sm btn-primary">Detail</a>
                          <a href="{{ url('pegawai/tagihan/cetak/'.$d['tiket_id']) }}" class="btn btn-sm btn-primary" target="_blank">Cetak</a>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('script')
  @include('components.sweet_alert')
  @include('components.datatables')
@endsection