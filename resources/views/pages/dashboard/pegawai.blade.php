@extends('layouts.pegawai')

@section('title')
  Pegawai | Dashboard
@endsection

@section('css')
@endsection

@section('content')
  <section class="content-header ml-0 py-3 px-5">
    <h1>
      Dashboard
    </h1>
    <ol class="breadcrumb mt-3 pt-3 mr-3">
      <li><a href="{{ url('pegawai') }}">Home</a></li>
    </ol>
  </section>
  <section class="container-fluid px-5">
    <div class="row">
      <div class="col-lg-3 col-xs-6 px-2">
        <div class="small-box bg-aqua pb-3">
          <div class="inner">
            <h3>{{ $data_jumlah['pengaduan'] }}</h3>
            <p>Jumlah Pengaduan</p>
          </div>
          <div class="icon">
            <i class="fa fa-comments"></i>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6 px-2">
        <div class="small-box bg-green pb-3">
          <div class="inner">
            <h3>{{ $data_jumlah['pengembangan'] }}</h3>
            <p>Jumlah Pengembangan</p>
          </div>
          <div class="icon">
            <i class="fa fa-cog"></i>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6 px-2">
        <div class="small-box bg-yellow pb-3">
          <div class="inner">
            <h3>{{ $data_jumlah['klien'] }}</h3>
            <p>Jumlah Klien</p>
          </div>
          <div class="icon">
            <i class="fa fa-users"></i>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6 px-2">
        <div class="small-box bg-red pb-3">
          <div class="inner">
            <h3>Rp {{ number_format($data_jumlah['tagihan'], 0, ',', '.') }}</h3>
            <p>Total Pembayaran</p>
          </div>
          <div class="icon">
            <i class="fa fa-money"></i>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <form class="col-sm-12 px-2" action="" method="get">
        <div class="box box-solid mt-3 mb-0">
          <div class="box-body px-4 mt-3">
            <select name="tahun" title="tahun" class="form-control" onchange="this.form.submit()">
              @foreach($data_tahun as $d)
                <option value="{{ $d }}" {{ $tahun == $d ? 'selected' : '' }}>Tahun {{ $d }}</option>
              @endforeach
            </select>
          </div>
        </div>
      </form>
    </div>
    <div class="row">
      <div class="col-sm-8 px-2">
        <div class="box box-solid box-primary mt-3">
          <div class="box-header with-border">
            Jumlah Pengaduan & Pengembangan Tahun {{ $tahun }}
          </div>
          <div class="box-body px-4">
            <div class="card-body d-flex p-0" id="chart-pengaduan"></div>
          </div>
        </div>
      </div>
      <div class="col-sm-4 px-2">
        <div class="box box-solid box-primary mt-3">
          <div class="box-header with-border">
            Jumlah Pengaduan Berdasarkan Layanan
          </div>
          <div class="box-body px-4 my-3">
            <div class="card-body d-flex p-0" id="chart-layanan"></div>
            <table class="table mt-5">
              @foreach($chart_layanan['nama'] as $index=>$d)
                <tr>
                  <td>{{ $d }}</td>
                  <td>{{ $chart_layanan['jumlah'][$index] }}</td>
                </tr>
              @endforeach
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('script')
  @include('components.sweet_alert')
  <script src="{{ asset('plugins/chart.js/Chart.bundle.js') }}"></script>
  <script>
      let colors = [
          '#EF5350',
          '#AB47BC',
          '#5C6BC0',
          '#42A5F5',
          '#26A69A',
          '#66BB6A',
          '#D4E157',
          '#FFA726',
          '#78909C',
      ];

      let donutOptions = {
          cutoutPercentage: 0,
          legend: {
              position:'bottom',
              labels:{
                  pointStyle:'circle',
                  usePointStyle:true,
              },
          },
      };
      let barOptions2 = {
          scales: {
              xAxes: [{
                  barPercentage: 0.4,
                  categoryPercentage: 0.5
              }],
              yAxes: [{
                  ticks: {
                      beginAtZero: false
                  }
              }]
          },
          legend: {
              display: true
          }
      };
      function setChartPengaduan(data_jumlah){
          // let width = $("#chart-jumlah-pesan").width();
          // let height = parseInt($("#table-pesan").height()) + 18;
          let width = 1000;
          let height = 700;
          let data = {
              labels: data_jumlah.bulan,
              datasets: [data_jumlah.pengaduan, data_jumlah.pengembangan]
          };
          $("#chart-pengaduan").html("<canvas id='chart-pengaduan-1' width='"+width+"' height='"+height+"'/>");
          new Chart($("#chart-pengaduan-1"), {
              type: 'horizontalBar',
              data: data,
              options: barOptions2
          });
      }
      function setChartLayanan(nama, jumlah) {
          let data = {
              labels: nama,
              datasets: [{
                  backgroundColor: colors,
                  borderWidth: 0,
                  data: jumlah
              }]
          };
          $('#chart-layanan').html("<canvas id='chart-layanan1' width='400' height='350'/>");
          new Chart($('#chart-layanan1'), {
              type: 'pie',
              data: data,
              options: donutOptions
          });
      }

      setChartPengaduan(JSON.parse('{!! json_encode($chart_pengaduan) !!}'));
      setChartLayanan(JSON.parse('{!! json_encode($chart_layanan['nama']) !!}'), JSON.parse('{!! json_encode($chart_layanan['jumlah']) !!}'));
  </script>
@endsection