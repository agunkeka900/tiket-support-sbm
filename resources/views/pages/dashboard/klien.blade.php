@extends('layouts.klien')

@section('title')
  Klien | Dashboard
@endsection

@section('css')
  <link rel="stylesheet" href="{{ url('plugins/dropify/css/dropify.css') }}">
@endsection

@section('content')
  <div class="content-wrapper ml-0">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <section class="content-header ml-0 py-5">
            <h1>Dashboard</h1>
          </section>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              @foreach($tiket_jenis as $index=>$j)
                <li class="{{ $index == 0 ? 'active' : '' }}"><a href="#tab{{ $j->tiket_jenis_id }}" data-toggle="tab" aria-expanded="true">{{ $j->nama }}</a></li>
              @endforeach
            </ul>
            <div class="tab-content">
              @foreach($tiket_jenis as $index=>$j)
                <div class="tab-pane px-3 {{ $index == 0 ? 'active' : '' }}" id="tab{{ $j->tiket_jenis_id }}">
                  @foreach($layanan as $l)
                    <div class="box box-solid box-primary mt-3">
                      <div class="box-header with-border">
                        <div class="row px-4">
                          <div class="col-sm-6 pt-2">
                            <h3 class="box-title mt-1">{{ $l['nama'] }}</h3>
                          </div>
                          <div class="col-sm-6">
                            <button data-toggle="modal" data-target="#modal{{ $j->tiket_jenis_id.'-'.$l->layanan_id }}"
                                    type="button" class="btn btn-default float-right">Tambah {{ $j->nama }}</button>
                          </div>
                        </div>
                      </div>
                      <div class="box-body px-4">
                        <div class="row px-2">
                          <div class="col-sm-4 px-2">
                            <a href="{{ url('klien/menunggu/'.$j->tiket_jenis_id.'/'.$l->layanan_id) }}" class="info-box bg-green mb-0">
                              <span class="info-box-icon"><i class="fa fa-hourglass-start"></i></span>
                              <div class="info-box-content pt-4">
                                <span class="info-box-text">Menunggu</span>
                                <span class="info-box-number" style="font-size: 22pt">{{ $l[$j->nama]['menunggu'] }}</span>
                              </div>
                            </a>
                          </div>
                          <a href="{{ url('klien/diproses/'.$j->tiket_jenis_id.'/'.$l->layanan_id) }}" class="col-sm-4 px-2">
                            <div class="info-box bg-yellow mb-0">
                              <span class="info-box-icon"><i class="fa fa-wrench"></i></span>
                              <div class="info-box-content pt-4">
                                <span class="info-box-text">Diproses</span>
                                <span class="info-box-number" style="font-size: 22pt">{{ $l[$j->nama]['diproses'] }}</span>
                              </div>
                            </div>
                          </a>
                          <div class="col-sm-4 px-2">
                            <a href="{{ url('klien/selesai/'.$j->tiket_jenis_id.'/'.$l->layanan_id) }}" class="info-box bg-red mb-0">
                              <span class="info-box-icon"><i class="fa fa-check"></i></span>
                              <div class="info-box-content pt-4">
                                <span class="info-box-text">Selesai</span>
                                <span class="info-box-number" style="font-size: 22pt">{{ $l[$j->nama]['selesai'] }}</span>
                              </div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  @endforeach
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @foreach($tiket_jenis as $j)
    @foreach($layanan as $l)
      <div id="modal{{ $j->tiket_jenis_id.'-'.$l->layanan_id }}" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
              <h4 class="modal-title">Tambah {{ $j->nama }}</h4>
            </div>
            <form action="{{ url('pengaduan') }}" method="post" enctype="multipart/form-data">
              <div class="modal-body">
                @csrf
                <input type="hidden" name="layanan_id" value="{{ $l->layanan_id }}">
                <input type="hidden" name="tiket_jenis_id" value="{{ $j->tiket_jenis_id }}">
                <div class="form-group">
                  <label for="layanan_id">Jenis Layanan</label>
                  <div>
                    <input type="text" id="layanan_id" class="form-control" readonly value="{{ $j->nama }}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="nama_layanan">Nama Layanan</label>
                  <div>
                    <input type="text" id="nama_layanan" class="form-control" readonly value="{{ $l->nama }}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="judul">Judul</label>
                  <div>
                    <input type="text" name="judul" id="judul" class="form-control" required autofocus>
                  </div>
                </div>
                <div class="form-group">
                  <label for="deskripsi">Deskripsi</label>
                  <div>
                    <textarea name="deskripsi" id="deskripsi" class="form-control" rows="5" required></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="url">Foto</label>
                  <div>
                    <input type="file"
                           name="foto"
                           class="dropify dropify-photo"
                           data-height="150"
                           data-show-remove="false"
                           data-allowed-file-extensions="png jpg jpeg"
                           data-max-file-size="1M"
                           accept="image/jpeg, image/jpg, image/png" />
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    @endforeach
  @endforeach
@endsection
@section('script')
  @include('components.sweet_alert')
  <script src="{{ url('plugins/dropify/js/dropify.min.js') }}"></script>
  <script>
      $(document).ready(function(){
          $('.dropify-photo').dropify({
              messages: {
                  'default': 'Drag & drop gambar di sini atau klik',
                  'replace': 'Drag & drop atau klik untuk mengganti gambar',
                  'error': 'Hanya format png, jpg, jpeg yang di izinkan'
              },
              error: {
                  'fileSize': 'Ukuran gambar melebihi maksimal (1M max).',
              }
          });
      });
  </script>
@endsection