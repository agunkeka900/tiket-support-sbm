@extends('layouts.pegawai')

@section('title')
  Pegawai | Arsip Data
@endsection

@section('css')
@endsection

@section('content')
  <section class="content-header ml-0 pb-4 pt-5 px-5">
    <h1>
      Arsip Data
    </h1>
    <ol class="breadcrumb mt-3 pt-3 mr-3">
      <li><a href="{{ url('pegawai') }}">Home</a></li>
      <li class="active">Arsip Data</li>
    </ol>
  </section>
  <section class="container-fluid px-5">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-primary mt-3">
          <form class="box-body px-4">
            <div class="row px-3" style="border-bottom: 1px solid #D2D6DE">
              <div class="col-sm-4 px-2 pb-3">
                <select name="date_option" id="date_option" title="Berdasarkan" class="form-control" onchange="this.form.submit()">
                  <option value="bulan" {{ $date_option == 'bulan' ? 'selected' : '' }}>Berdasarkan Bulan</option>
                  <option value="tanggal" {{ $date_option == 'tanggal' ? 'selected' : '' }}>Berdasarkan Tanggal</option>
                </select>
              </div>
              <div class="col-sm-4 px-2 pb-3">
                @if($date_option == 'bulan')
                  <select name="bulan" title="Bulan" class="form-control" onchange="this.form.submit()">
                    <option value="">Pilih Bulan</option>
                    <option value="all" {{ $bulan == 'all' ? 'selected' : '' }}>Semua Bulan</option>
                    @foreach($data_bulan as $d)
                      <option value="{{ $d }}" {{ $bulan == $d ? 'selected' : '' }}>Bulan {{ \App\Http\Controllers\HelperController::setNamaBulan($d) }}</option>
                    @endforeach
                  </select>
                @endif
                @if($date_option == 'tanggal')
                  <input type="date" name="tanggal" value="{{ $tanggal }}" class="form-control" title="Tanggal" onchange="this.form.submit()">
                @endif
              </div>
            </div>
            <div class="row px-3 py-3">
              <div class="col-sm-4 px-2 pb-3">
                <select name="tiket_jenis_id" title="Jenis Layanan" class="form-control" onchange="this.form.submit()">
                  <option value="">Pilih Jenis Layanan</option>
                  <option value="all" {{ $tiket_jenis_id == 'all' ? 'selected' : '' }}>Semua Jenis Layanan</option>
                  @foreach($data_jenis_layanan as $d)
                    <option value="{{ $d['tiket_jenis_id'] }}" {{ $tiket_jenis_id == $d['tiket_jenis_id'] ? 'selected' : '' }}>Jenis Layanan {{ $d['nama'] }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-sm-4 px-2 pb-3">
                <select name="layanan_id" title="Nama Layanan" class="form-control" onchange="this.form.submit()">
                  <option value="">Pilih Layanan</option>
                  <option value="all" {{ $layanan_id == 'all' ? 'selected' : '' }}>Semua Layanan</option>
                  @foreach($data_layanan as $d)
                    <option value="{{ $d['layanan_id'] }}" {{ $layanan_id == $d['layanan_id'] ? 'selected' : '' }}>Layanan {{ $d['nama'] }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-sm-4 px-2 pb-3">
                <select name="status" title="status" class="form-control" onchange="this.form.submit()">
                  <option value="">Pilih Status</option>
                  <option value="all" {{ $status == 'all' ? 'selected' : '' }}>Semua Status</option>
                  @foreach($data_status as $d)
                    <option value="{{ $d }}" {{ $status == $d ? 'selected' : '' }}>Status {{ ucwords($d) }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-sm-4 px-2">
                <select name="pegawai_id" title="Pegawai" class="form-control" onchange="this.form.submit()">
                  <option value="">Pilih Pegawai</option>
                  <option value="all" {{ $pegawai_id == 'all' ? 'selected' : '' }}>Semua Pegawai</option>
                  @foreach($data_pegawai as $d)
                    <option value="{{ $d['pegawai_id'] }}" {{ $pegawai_id == $d['pegawai_id'] ? 'selected' : '' }}>Pegawai {{ $d['nama'] }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-sm-4 px-2">
                <select name="status_bayar" title="Status Bayar" class="form-control" onchange="this.form.submit()">
                  <option value="">Pilih Status Bayar</option>
                  <option value="all" {{ $status_bayar == 'all' ? 'selected' : '' }}>Semua Status Bayar</option>
                  @foreach($data_status_bayar as $d)
                    <option value="{{ $d }}" {{ $status_bayar == $d ? 'selected' : '' }}>{{ $d }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-sm-4 px-2">
                <select name="pic" title="History PIC" class="form-control" onchange="this.form.submit()">
                  <option value="all">Histori PIC tidak ditentukan</option>
                  <option value="0" {{ $pic == '0' ? 'selected' : '' }}>Tanpa perubahan PIC</option>
                  <option value="2" {{ $pic == '2' ? 'selected' : '' }}>Perubahan PIC lebih dari satu kali</option>
                  <option value="3" {{ $pic == '3' ? 'selected' : '' }}>Perubahan PIC lebih dari dua kali</option>
                </select>
              </div>
              <div class="col-sm-12 text-right px-2 pt-4">
                @php
                  $param = 'tiket_jenis_id='.$tiket_jenis_id.
                    '&date_option='.$date_option.
                    '&bulan='.$bulan.
                    '&tanggal='.$tanggal.
                    '&layanan_id='.$layanan_id.
                    '&status='.$status.
                    '&pegawai_id='.$pegawai_id.
                    '&status_bayar='.$status_bayar;
                @endphp
                <a class="btn btn-primary" href="{{ url('pegawai/arsip/cetak?'.$param) }}" target="_blank">
                  <i class="fa fa-print mr-2"></i> Cetak Laporan
                </a>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="col-sm-12">
        <div class="box box-solid mt-0 pt-2">
          <div class="box-body px-2 py-4">
            <div class="row">
              <div class="col-sm-12 pb-2 px-5 pt-0">
                <div class="table-responsive">
                  <table id="datatable" class="table table-sm table-bordered ">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th style="min-width: 70px">ID Tiket</th>
                      <th style="min-width: 170px">Waktu Pengajuan</th>
                      @if($tiket_jenis_id == 'all')
                        <th style="min-width: 100px">Jenis</th>
                      @endif
                      @if($layanan_id == 'all')
                        <th>Layanan</th>
                      @endif
                      @if($status == 'all')
                        <th>Status</th>
                      @endif
                      @if($pegawai_id == 'all')
                        <th>Pegawai</th>
                      @endif
                      <th style="min-width: 200px">Judul</th>
                      <th>Foto</th>
                      <th>Klien</th>
                      <th style="min-width: 300px">Deskripsi</th>
                      @if($status_bayar != \App\Tagihan::S_TIDAK)
                        <th>Pembayaran</th>
                        <th style="min-width: 120px">Date</th>
                        <th style="min-width: 120px">Due Date</th>
                        <th style="min-width: 150px">Sisa Pembayaran</th>
                      @endif
                      @if($pic == 2 || $pic == 3)
                        <th style="min-width: 450px">History PIC</th>
                      @endif
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $no=>$d)
                      <tr>
                        <td class="text-center">{{ $no+1 }}</td>
                        <td>{{ $d['tiket_id'] }}</td>
                        <td>{{ \App\Http\Controllers\HelperController::setNamaWaktu($d['created_at']) }}</td>
                        @if($tiket_jenis_id == 'all')
                          <td>{{ $d['tiket_jenis'] }}</td>
                        @endif
                        @if($layanan_id == 'all')
                          <td>{{ $d['nama_layanan'] }}</td>
                        @endif
                        @if($status == 'all')
                          <td>{{ $d['status'] }}</td>
                        @endif
                        @if($pegawai_id == 'all')
                          <td>{{ $d['pegawai'] }}</td>
                        @endif
                        <td>{{ $d['judul'] }}</td>
                        <td class="p-0 text-center">
                          @if($d['url'] != null)
                            <img src="{{ url($d['url']) }}" alt="Foto" height="50px" data-toggle="modal" data-target="#modal{{ $d->tiket_id }}" style="cursor: pointer">
                          @endif
                        </td>
                        <td>{{ $d['nama_klien'] }}</td>
                        <td>{{ $d['deskripsi'] }}</td>
                        @if($status_bayar != \App\Tagihan::S_TIDAK)
                          <td class="text-right">{{ $d['pembayaran'] != null ? number_format($d['pembayaran'], 0, ',', '.') : '' }}</td>
                          <td>{{ \App\Http\Controllers\HelperController::setNamaBulan(null, $d['date']) }}</td>
                          <td>{{ \App\Http\Controllers\HelperController::setNamaBulan(null, $d['due_date']) }}</td>
                          <td class="text-right">{{ $d['sisa_pembayaran'] != null ? number_format($d['sisa_pembayaran'], 0, ',', '.') : '' }}</td>
                        @endif
                        @if($pic == 2 || $pic == 3)
                          <td>
                            @php
                              $arr_pic = [];
                              foreach($d['history_pic'] as $p){
                                $tgl = \App\Http\Controllers\HelperController::setNamaBulan(null, substr($p['created_at'], 0, 10));
                                $arr_pic[] = "($p->nama | Tgl: $tgl)";
                              }
                            @endphp
                            {{ implode(', ', $arr_pic) }}
                          </td>
                        @endif
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @foreach($data as $d)
    @include('components.pengaduan.modal_foto_pengaduan')
  @endforeach
@endsection
@section('script')
  @include('components.sweet_alert')
  @include('components.datatables')
@endsection