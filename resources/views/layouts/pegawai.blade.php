<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title')</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  {{--<link rel="stylesheet" href="{{ url('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">--}}
  <link rel="stylesheet" href="{{ url('bower_components/bootstrap2/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ url('bower_components/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ url('bower_components/Ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ url('css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ url('bower_components/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ url('css/skins/_all-skins.min.css') }}">
  @yield('css')
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <a href="{{ url('pegawai') }}" class="logo">
      <span class="logo-lg"><b>TIKET SUPPORT</b></span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          @php
            $data_notif = \App\Notifikasi::getNotifikasi();
            $jumlah_belum_dibaca = \App\Notifikasi::getJumlahBelumDibaca();
            $jumlah_semua = \App\Notifikasi::getJumlahNotif();
          @endphp
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              @if($jumlah_belum_dibaca > 0)
                <span class="label label-danger">{{ $jumlah_belum_dibaca }}</span>
              @endif
            </a>
            <ul class="dropdown-menu" style="width: 400px">
              @if($jumlah_belum_dibaca > 0)
                <li class="header text-center" style="color: #888888; font-size: 12px">Anda memiliki {{ $jumlah_belum_dibaca }} notifikasi belum dibaca</li>
              @endif
              <li>
                <ul class="menu" style="max-height: 400px !important;">
                  @foreach($data_notif as $d)
                    <li style="{{ $d->dibaca == \App\Notifikasi::D_BELUM ? 'background: #EBFFEF' : '' }}">
                      <a href="{{ url('notifikasi/read/'.$d->notif_id.'?redirect_link='.$d->redirect_link) }}" style="white-space: normal !important;">
                        <h4 class="ml-0">
                          {{ $d->judul }}
                        </h4>
                        <p class="ml-0 mt-2">{{ $d->keterangan }}</p>
                        <p class="ml-0 mt-2 text-right" style="font-size: 10px">
                          <i class="fa fa-clock-o"></i> {{ \App\Http\Controllers\HelperController::setNamaWaktu($d->created_at) }}
                        </p>
                      </a>
                    </li>
                  @endforeach
                </ul>
              </li>
              <li class="footer">
                <div class="row px-5 py-3">
                  <div class="col-sm-6">
                    <form action="{{ url('notifikasi/read-all') }}" method="post" id="form-read-all">
                      @csrf @method('put')
                      <a href="#" onclick="document.getElementById('form-read-all').submit()"
                         style="color: #337ab7 !important; font-size: 12px">Tandai semua dibaca</a>
                    </form>
                  </div>
                  <div class="col-sm-6 text-right">
                    <a href="{{ url('notifikasi/pegawai') }}" style="color: #337ab7 !important; font-size: 12px">Lihat selengkapnya ({{ $jumlah_semua }})</a>
                  </div>
                </div>
              </li>
            </ul>
          </li>

          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              {{--<img src="assets/dist/img/user2-160x160.jpg" class="user-image" alt="">--}}
              <span class="hidden-xs">{{auth()->user()->username}}</span>
            </a>

            <ul class="dropdown-menu">
              <li class="user-header" style="background: white; height: 140px; margin-top: 20px">
                <img src="{{ url('img/logo.jpg') }}" alt="User Image" style="width: 200px; height: 100px">
                <p>
                  <span class="hidden-xs"></span>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-right-logout">
                  <form action="{{ url('logout') }}" method="post">
                    @csrf
                    <button class="btn btn-primary btn-block">Sign out</button>
                  </form>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ url('img/profil-teknisi.jpg') }}" class="img-circle" alt="">
        </div>
        <div class="pull-left info pt-4">
          <span class="hidden-xs" >{{auth()->user()->username}}</span>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
        <li>
          <a href="{{ url('pegawai') }}">
            <i class="fa fa-home"></i>
            <span>Home</span>
          </a>
        </li>
        <li>
          <a href="{{ url('pegawai/pengaduan') }}">
            <i class="fa fa-comment"></i>
            <span>Pengaduan</span>
          </a>
        </li>
        <li>
          <a href="{{ url('pegawai/pengembangan') }}">
            <i class="fa fa-window-restore"></i>
            <span>Pengembangan</span>
          </a>
        </li>
        <li>
          <a href="{{ url('pegawai/arsip') }}">
            <i class="fa fa-file"></i>
            <span>Arsip Data</span>
          </a>
        </li>
        <li>
          <a href="{{ url('pegawai/tagihan') }}">
            <i class="fa fa-money"></i>
            <span>Tagihan</span>
          </a>
        </li>
      </ul>
    </section>
  </aside>

  <div class="content-wrapper">
    <div>
      @yield('content')
    </div>
  </div>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; {{ date('Y') }} CV. Sari Bhakti Meening.</strong> Crafted with <i class="fa fa-heart-o text-red"></i> by <span class="text-blue">Era</span>
  </footer>
</div>

<script src="{{ url('bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ url('bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ url('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ url('js/adminlte.min.js') }}"></script>
<script src="{{ url('js/vendor.min.js') }}"></script>
<script src="{{ url('js/app.min.js') }}"></script>
@yield('script')
</body>
</html>
