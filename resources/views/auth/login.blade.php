@extends('layouts.login')

@section('content')
  <div class="auth-box ">
    <div class="left">
      <div class="content">
        <div class="header">
          <div class="logo text-center"><img src="{{ asset('img/logo-dark1.jpg') }}" alt="Tiket Logo"></div>
          <p class="lead">Login ke dashboard</p>
        </div>
        @if (session('error'))
          <div class="alert alert-danger">
            {{ session('error') }}
          </div>
        @endif
        <form action="{{ url('login') }}" method="post">
          @csrf
          <div class="form-group">
            <label for="username" class="control-label sr-only">Username</label>
            <input type="text" class="form-control" name="username" id="username"  placeholder="Username">
          </div>
          <div class="form-group">
            <label for="password" class="control-label sr-only">Password</label>
            <input type="password" class="form-control" name="password" id="password"  placeholder="Password">
          </div>

          <button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>

        </form>
      </div>
    </div>
    <div class="right">
      <div class="overlay"></div>
      <div class="content text">
        <h1 class="heading">Segera Laporkan Keluhan Anda</h1>
        <p>Masuk ke Dashboard Anda</p>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>


  {{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
      {{--<div class="col-md-8">--}}
        {{--<div class="card">--}}
          {{--<div class="card-header">{{ __('Login') }}</div>--}}

          {{--<div class="card-body">--}}
            {{--<form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">--}}
              {{--@csrf--}}

              {{--<div class="form-group row">--}}
                {{--<label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                {{--<div class="col-md-6">--}}
                  {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>--}}

                  {{--@if ($errors->has('email'))--}}
                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                  {{--@endif--}}
                {{--</div>--}}
              {{--</div>--}}

              {{--<div class="form-group row">--}}
                {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                {{--<div class="col-md-6">--}}
                  {{--<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

                  {{--@if ($errors->has('password'))--}}
                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                  {{--@endif--}}
                {{--</div>--}}
              {{--</div>--}}

              {{--<div class="form-group row">--}}
                {{--<div class="col-md-6 offset-md-4">--}}
                  {{--<div class="form-check">--}}
                    {{--<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

                    {{--<label class="form-check-label" for="remember">--}}
                      {{--{{ __('Remember Me') }}--}}
                    {{--</label>--}}
                  {{--</div>--}}
                {{--</div>--}}
              {{--</div>--}}

              {{--<div class="form-group row mb-0">--}}
                {{--<div class="col-md-8 offset-md-4">--}}
                  {{--<button type="submit" class="btn btn-primary">--}}
                    {{--{{ __('Login') }}--}}
                  {{--</button>--}}

                  {{--<a class="btn btn-link" href="{{ route('password.request') }}">--}}
                    {{--{{ __('Forgot Your Password?') }}--}}
                  {{--</a>--}}
                {{--</div>--}}
              {{--</div>--}}
            {{--</form>--}}
          {{--</div>--}}
        {{--</div>--}}
      {{--</div>--}}
    {{--</div>--}}
  {{--</div>--}}
@endsection
@section('script')
  @include('components.sweet_alert')
@endsection
