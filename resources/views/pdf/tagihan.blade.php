<title>Tagihan</title>


<table>
  <tr>
    <td style="width: 250px">
      <img src="{{ url('img/logo.jpg') }}" width="150px" alt="Logo">
    </td>
    <td>
      <table>
        <tr>
          <td>No Invoice</td>
          <td>:</td>
          <td>
            <div style="padding: 3px; background: #0b97c4; width: max-content; color: #fff; font-weight: bold">#{{ sprintf("%04d", $data['invoice_id']) }}</div>
          </td>
        </tr>
        <tr>
          <td>Date</td>
          <td>:</td>
          <td>{{ \App\Http\Controllers\HelperController::setNamaBulan(null, $data['date']) }}</td>
        </tr>
        <tr>
          <td>Due Date</td>
          <td>:</td>
          <td>{{ \App\Http\Controllers\HelperController::setNamaBulan(null, $data['due_date']) }}</td>
        </tr>
        <tr>
          <td colspan="3">
            <div style="padding: 3px; background: #FF9A00; width: max-content; color: #fff; font-weight: bold">{{ $data_tiket['tiket_jenis'] }} Produk</div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<table class="table mt-5">
  <tr>
    <td class="border-bottom">Info Perusahaan</td>
    <td class="border-bottom">Info Pelanggan</td>
  </tr>
  <tr>
    <td>
      <span class="text-primary font-weight-bold">CV. Sari Bhakti Meening</span>
    </td>
    <td>
      <span class="text-primary font-weight-bold">{{ $data_tiket['nama_klien'] }}</span>
    </td>
  </tr>
  <tr>
    <td>
      Phone: +6287861454109 <br>
      Website: http://www.sbm.co.id <br>
      Alamat: Jl. Sedap malam gg. Tunjung Biru <br> IV/A No 16 Denpasar Timur
    </td>
    <td>
      No HP: {{ $data_tiket['no_hp'] }} <br>
      Alamat: {{ $data_tiket['alamat'] }}
    </td>
  </tr>
  <tr>
    <td colspan="2" class="border-bottom"> </td>
  </tr>
</table>

<table class="mt-5" style="width: 100%; font-size: 10pt !important;">
  <tr>
    <td style="width: 150px">Judul Pengaduan</td>
    <td style="width: 5px">:</td>
    <td>{{ $data_tiket['judul'] }}</td>
  </tr>
  <tr>
    <td>Deskripsi</td>
    <td>:</td>
    <td>{{ $data_tiket['deskripsi'] }}</td>
  </tr>
  <tr>
    <td>Layanan</td>
    <td>:</td>
    <td>{{ $data_tiket['nama_layanan'] }}</td>
  </tr>
  <tr>
    <td>Pegawai</td>
    <td>:</td>
    <td>{{ $data_tiket['pegawai'] }}</td>
  </tr>
  <tr>
    <td>Harga</td>
    <td>:</td>
    <td class="font-weight-bold">Rp {{ number_format($data['pembayaran'], 0, ',', '.') }}</td>
  </tr>
</table>

<table class="table" style="margin-top: 130px">
  <tr>
    <td></td>
    <td style="width: 100px" class="text-center">
      <img src="{{ url('img/bca.png') }}" width="70" alt="BCA"><br><br>
      No Rek<br>
      98234412
    </td>
    <td style="width: 100px" class="text-center">
      <img src="{{ url('img/bni.png') }}" width="70" alt="BNI"><br><br>
      No Rek<br>
      7893684568239
    </td>
    <td></td>
  </tr>
</table>

<style>
  *{
    font-size: 8pt;
    font-family: sans-serif;
  }
  @if($data->sisa_pembayaran == 0)
    body {
      background-image: url({{ url('img/paid-stamp.png') }});
      background-position: top left;
      background-repeat: no-repeat;
      background-size: 100%;
    }
  @endif
  h1{
    text-align: center;
    font-size: 20px;
  }
  .table{
   width: 100%;
    border-spacing: 0;
  }
  .table tr td{
    padding: 5px;
    vertical-align: top;
    line-height: 12pt;
  }
  .body{
    width: 100%;
    margin-top: 10px;
    border-spacing: 0;
  }
  .body tr th{
    padding: 5px;
    border-left: 1px solid black;
    border-top: 1px solid black;
    border-bottom: 1px solid black;
  }
  .body tr td{
    padding: 5px;
    border-left: 1px solid black;
    border-bottom: 1px solid black;
    vertical-align: top;
  }
  .body tr td:last-child, .body tr th:last-child{
    border-right: 1px solid black;
  }
  .border-bottom{
    border-bottom: 1px solid black;
  }
  .text-center{
    text-align: center;
  }
  .text-right{
    text-align: right;
  }
  .p-0{
    padding: 0 !important;
  }
  .mt-5{
    margin-top: 20px;
  }
  .text-primary{
    color: #0b97c4;
  }
  .font-weight-bold{
    font-weight: bold;
  }
</style>