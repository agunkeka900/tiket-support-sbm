<title>Laporan Arsip</title>

<h1>Laporan Arsip</h1>
<table>
  @if($date_option == 'bulan')
  <tr>
    <td>Bulan</td>
    <td>: {{ $bulan == 'all' ? 'Semua Bulan' : $bulan }}</td>
  </tr>
  @endif
  @if($date_option == 'tanggal')
    <tr>
      <td>Tanggal</td>
      <td>: {{ $tanggal }}</td>
    </tr>
  @endif
  <tr>
    <td>Jenis Layanan</td>
    <td>: {{ $jenis_layanan }}</td>
  </tr>
  <tr>
    <td>Nama Layanan</td>
    <td>: {{ $nama_layanan }}</td>
  </tr>
  <tr>
    <td>Status</td>
    <td>: {{ $status == 'all' ? 'Semua Status' : ucwords($status) }}</td>
  </tr>
  <tr>
    <td>Pegawai</td>
    <td>: {{ $pegawai }}</td>
  </tr>
  <tr>
    <td>Status Bayar</td>
    <td>: {{ $status_bayar == 'all' ? 'Semua Status Bayar' : $status_bayar }}</td>
  </tr>
  @if($pic != 'all')
    <tr>
      <td>Histori PIC</td>
      @if($pic == 0)
        <td>: Tanpa perubahan PIC</td>
      @elseif($pic == 2)
        <td>: Perubahan PIC lebih dari satu kali</td>
      @elseif($pic == 3)
        <td>: Perubahan PIC lebih dari dua kali</td>
      @endif
    </tr>
  @endif
</table>

<table class="body">
  <thead>
  <tr>
    <th>No</th>
    <th style="min-width: 100px">Waktu Pengajuan</th>
    @if($tiket_jenis_id == 'all')
      <th>Jenis</th>
    @endif
    @if($layanan_id == 'all')
      <th>Layanan</th>
    @endif
    @if($status == 'all')
      <th>Status</th>
    @endif
    @if($pegawai_id == 'all')
      <th>Pegawai</th>
    @endif
    <th>Judul</th>
    <th>Foto</th>
    <th>Klien</th>
    <th>Deskripsi</th>
    @if($status_bayar != \App\Tagihan::S_TIDAK)
      <th>Pembayaran</th>
      <th>Date</th>
      <th>Due Date</th>
      <th>Sisa Pembayaran</th>
    @endif
    @if($pic == 2 || $pic == 3)
      <th>History PIC</th>
    @endif
  </tr>
  </thead>
  <tbody>
  @foreach($data as $no=>$d)
    <tr>
      <td class="text-center">{{ $no+1 }}</td>
      <td>{{ \App\Http\Controllers\HelperController::setNamaWaktu($d['created_at']) }}</td>
      @if($tiket_jenis_id == 'all')
        <td>{{ $d['tiket_jenis'] }}</td>
      @endif
      @if($layanan_id == 'all')
        <td>{{ $d['nama_layanan'] }}</td>
      @endif
      @if($status == 'all')
        <td>{{ $d['status'] }}</td>
      @endif
      @if($pegawai_id == 'all')
        <td>{{ $d['pegawai'] }}</td>
      @endif
      <td>{{ $d['judul'] }}</td>
      <td class="p-0 text-center">
        @if($d['url'] != null)
          <img src="{{ url($d['url']) }}" alt="Foto" height="40px" data-toggle="modal" data-target="#modal{{ $d->tiket_id }}" style="cursor: pointer">
        @endif
      </td>
      <td>{{ $d['nama_klien'] }}</td>
      <td>{{ $d['deskripsi'] }}</td>
      @if($status_bayar != \App\Tagihan::S_TIDAK)
        <td class="text-right">{{ $d['pembayaran'] != null ? number_format($d['pembayaran'], 0, ',', '.') : '' }}</td>
        <td>{{ \App\Http\Controllers\HelperController::setNamaBulan(null, $d['date']) }}</td>
        <td>{{ \App\Http\Controllers\HelperController::setNamaBulan(null, $d['due_date']) }}</td>
        <td class="text-right">{{ $d['sisa_pembayaran'] != null ? number_format($d['sisa_pembayaran'], 0, ',', '.') : '' }}</td>
      @endif
      @if($pic == 2 || $pic == 3)
        <td>
          @php
            $arr_pic = [];
            foreach($d['history_pic'] as $p){
              $tgl = \App\Http\Controllers\HelperController::setNamaBulan(null, substr($p['created_at'], 0, 10));
              $arr_pic[] = "($p->nama | Tgl: $tgl)";
            }
          @endphp
          {{ implode(', ', $arr_pic) }}
        </td>
      @endif
    </tr>
  @endforeach
  </tbody>
</table>

<style>
  @page{
    margin: 0.5cm;
    size: 33cm 21cm;
  }
  *{
    font-size: 8pt;
    font-family: sans-serif;
  }
  h1{
    text-align: center;
    font-size: 20px;
  }
  .body{
    width: 100%;
    margin-top: 10px;
    border-spacing: 0;
  }
  .body tr th{
    padding: 5px;
    border-left: 1px solid black;
    border-top: 1px solid black;
    border-bottom: 1px solid black;
  }
  .body tr td{
    padding: 5px;
    border-left: 1px solid black;
    border-bottom: 1px solid black;
    vertical-align: top;
  }
  .body tr td:last-child, .body tr th:last-child{
    border-right: 1px solid black;
  }
  /*.body tr:last-child td{*/
    /*border-bottom: 1px solid black;*/
  /*}*/
  .text-center{
    text-align: center;
  }
  .text-right{
    text-align: right;
  }
  .p-0{
    padding: 0 !important;
  }
</style>