<?php
Route::get('/','Auth\LoginController@showLoginForm');
Route::get('login','Auth\LoginController@showLoginForm')->name('login');
Route::post('login','Auth\LoginController@login');
Route::post('logout','Auth\LoginController@logout')->name('logout');

Route::middleware(['auth'])->group(function () {
  Route::get('/home', 'HomeController@index')->name('home');

  Route::get('klien','KlienController@index');
  Route::get('klien/menunggu/{tiket_jenis_id}/{layanan_id}','KlienController@viewMenunggu');
  Route::get('klien/diproses/{tiket_jenis_id}/{layanan_id}','KlienController@viewDiproses');
  Route::get('klien/selesai/{tiket_jenis_id}/{layanan_id}','KlienController@viewSelesai');
  Route::get('klien/diproses/detail/{tiket_jenis_id}/{tiket_id}','KlienController@viewDiprosesDetail');
  Route::get('klien/selesai/detail/{tiket_jenis_id}/{tiket_id}','KlienController@viewSelesaiDetail');

  Route::get('pegawai','PegawaiController@index');
  Route::get('pegawai/pengaduan','PegawaiController@viewPengaduan');
  Route::get('pegawai/pengaduan/menunggu/{layanan_id}','PegawaiController@viewPengaduanMenunggu');
  Route::get('pegawai/pengaduan/diproses/{layanan_id}','PegawaiController@viewPengaduanDiproses');
  Route::get('pegawai/pengaduan/diproses/detail/{tiket_id}','PegawaiController@viewPengaduanDiprosesDetail');
  Route::get('pegawai/pengaduan/selesai/{layanan_id}','PegawaiController@viewPengaduanSelesai');
  Route::get('pegawai/pengaduan/diproses/detail/{tiket_id}','PegawaiController@viewPengaduanDiprosesDetail');
  Route::get('pegawai/pengaduan/selesai/detail/{tiket_id}','PegawaiController@viewPengaduanSelesaiDetail');

  Route::get('pegawai/pengembangan','PegawaiController@viewPengembangan');
  Route::get('pegawai/pengembangan/menunggu/{layanan_id}','PegawaiController@viewPengembanganMenunggu');
  Route::get('pegawai/pengembangan/diproses/{layanan_id}','PegawaiController@viewPengembanganDiproses');
  Route::get('pegawai/pengembangan/diproses/detail/{tiket_id}','PegawaiController@viewPengembanganDiprosesDetail');
  Route::get('pegawai/pengembangan/selesai/{layanan_id}','PegawaiController@viewPengembanganSelesai');
  Route::get('pegawai/pengembangan/diproses/detail/{tiket_id}','PegawaiController@viewPengembanganDiprosesDetail');
  Route::get('pegawai/pengembangan/selesai/detail/{tiket_id}','PegawaiController@viewPengembanganSelesaiDetail');

  Route::post('pengaduan','PengaduanController@store');
  Route::put('pengaduan/pic','PengaduanController@updatePic');
  Route::put('pengaduan/update-progress','PengaduanController@updateProgress');
  Route::put('pengaduan/selesai-progress','PengaduanController@selesaiProgress');

  Route::post('comment','CommentController@store');

  Route::get('pegawai/tagihan','TagihanController@index');
  Route::get('pegawai/tagihan/edit/{tiket_id}','TagihanController@viewEdit');
  Route::get('pegawai/tagihan/cetak/{tiket_id}','TagihanController@cetak');
  Route::post('tagihan','TagihanController@store');
  Route::put('tagihan','TagihanController@update');

  Route::post('review/like','ReviewController@like');
  Route::post('review/dislike','ReviewController@dislike');

  Route::get('pegawai/arsip','ArsipDataController@index');
  Route::get('pegawai/arsip/cetak','ArsipDataController@cetak');

  Route::get('notifikasi/pegawai','NotifikasiController@indexPegawai');
  Route::get('notifikasi/klien','NotifikasiController@indexKlien');
  Route::get('notifikasi/read/{notif_id}','NotifikasiController@read');
  Route::put('notifikasi/read-all','NotifikasiController@readAll');
  Route::delete('notifikasi','NotifikasiController@delete');
});

Route::get('inisiasi-history-pic','PengaduanController@inisiasiPicHistory');
