<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiketStatusDiproses extends Model
{
  protected $table = 'tiket_status_diproses';
  protected $primaryKey = 'tiket_status_diproses_id';

  public static function getInfo($tiket_id)
  {
    return self::where('tiket_id', $tiket_id)->first();
  }

  public static function insertIfNotExists($data)
  {
    $exists = self::where('tiket_id', $data['tiket_id'])->exists();

    if(!$exists){
      $key = array_keys($data);
      $new = new self();
      foreach($key as $k){
        $new->$k = $data[$k];
      }
      $new->save();

      return $new->tiket_status_diproses_id;
    }
  }

  public static function updateData($data)
  {
    self::where('tiket_id', $data['tiket_id'])->update($data);
  }
}
