<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PicHistory extends Model
{
  protected $table = 'pic_history';
  protected $primaryKey = 'id_history';

  public static function insertData($data)
  {
    self::insert($data);
  }

  public static function getData($tiket_id)
  {
    return self::leftJoin('pegawai as p','p.pegawai_id','=','pic_history.pegawai_id')
      ->where('pic_history.tiket_id', $tiket_id)
      ->select('p.nama','pic_history.*')
      ->get();
  }

  public static function getDataAll()
  {
    return self::all();
  }

  public static function getJumlahHistory($tiket_id)
  {
    return self::where('tiket_id', $tiket_id)->count();
  }
}
