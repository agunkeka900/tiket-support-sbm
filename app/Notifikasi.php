<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Notifikasi extends Model
{
  protected $table = 'notifikasi';

  public const D_SUDAH = 'SUDAH';
  public const D_BELUM = 'BELUM';

  public static function getNotifikasi()
  {
    $user_id = Auth::user()->id;
    return self::where('user_id', $user_id)
      ->orderBy('created_at','DESC')
      ->limit(10)
      ->get();
  }

  public static function getAllNotifikasi()
  {
    $user_id = Auth::user()->id;
    return self::where('user_id', $user_id)
      ->orderBy('created_at','DESC')
      ->get();
  }

  public static function getJumlahBelumDibaca()
  {
    $user_id = Auth::user()->id;
    return self::where('user_id', $user_id)
      ->where('dibaca', self::D_BELUM)
      ->get()
      ->count();
  }

  public static function getJumlahNotif()
  {
    $user_id = Auth::user()->id;
    return self::where('user_id', $user_id)
      ->get()
      ->count();
  }

  public static function insertData($data)
  {
    $key = array_keys($data);
    $new = new self();
    foreach($key as $k){
      $new->$k = $data[$k];
    }
    $new->save();

    return $new->id;
  }

  public static function read($notif_id)
  {
    $user_id = Auth::user()->user_id;
    self::where('notif_id', $notif_id)
      ->where('user_id', $user_id)
      ->update([
        'dibaca' => self::D_SUDAH
      ]);
  }

  public static function readAll($user_id)
  {
    self::where('user_id', $user_id)
      ->update([
        'dibaca' => self::D_SUDAH
      ]);
  }

  public static function deleteData($notif_id, $user_id)
  {
    self::where('notif_id', $notif_id)
      ->where('user_id', $user_id)
      ->delete();
  }
}
