<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Pegawai extends Model
{
  protected $table = 'pegawai';
  protected $primaryKey = 'pegawai_id';

  public static function getNama($pegawai_id)
  {
    if($pegawai_id == 'all') return 'Semua Pegawai';
    $get = self::where('pegawai_id', $pegawai_id)->first();
    return $get ? $get->nama : null;
  }

  public static function getData()
  {
    return self::all();
  }

  public static function getPegawaiId()
  {
    $user_id = Auth::user()->user_id;
    $get = self::where('user_id', $user_id)->first();

    return $get ? $get->pegawai_id : null;
  }
}
