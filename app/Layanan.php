<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Layanan extends Model
{
  protected $table = 'layanan';
  protected $primaryKey = 'layanan_id';

  public static function getData()
  {
    return self::all();
  }

  public static function getNama($id)
  {
    if($id == 'all') return 'Semua Layanan';
    $get = self::find($id);
    return $get ? $get->nama : null;
  }
}
