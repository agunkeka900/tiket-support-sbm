<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tagihan extends Model
{
  protected $table = 'tagihan';
  protected $primaryKey = 'invoice_id';

  public const S_BERBAYAR = 'Berbayar';
  public const S_TIDAK = 'Tidak Berbayar';

  public static $status = [
    self::S_BERBAYAR, self::S_TIDAK
  ];

  public static function getData()
  {
    return self::leftJoin('tiket','tiket.tiket_id','=','tagihan.tiket_id')
      ->leftJoin('layanan as l','l.layanan_id','=','tiket.layanan_id')
      ->leftJoin('tiket_jenis as j','j.tiket_jenis_id','=','tiket.tiket_jenis_id')
      ->leftJoin('tiket_status as s','s.tiket_id' ,'=','tiket.tiket_id')
      ->leftJoin('tiket_pegawai as tp','tp.tiket_id' ,'=','tiket.tiket_id')
      ->leftJoin('users as u','u.username','=','tp.nama')
      ->leftJoin('pegawai as p','p.user_id','=','u.user_id')
      ->leftJoin('klien as k','k.klien_id','=','tiket.klien_id')
      ->select('tagihan.*','j.nama as tiket_jenis','s.nama as status','p.nama as pegawai',
        'k.nama as nama_klien','l.nama as nama_layanan')
      ->get();
  }

  public static function getTotal()
  {
    return self::selectRaw("SUM(pembayaran) as total")->first()->total;
  }

  public static function getInfo($tiket_id)
  {
    return self::leftJoin('klien as k','k.klien_id','=','tagihan.klien_id')
      ->where('tagihan.tiket_id', $tiket_id)
      ->select('tagihan.*','k.nama as nama_klien')
      ->first();
  }

  public static function insertData($data)
  {
    $exists = self::where('tiket_id', $data['tiket_id'])->first();
    $key = array_keys($data);
    if($exists){
      foreach($key as $k){
        $exists->$k = $data[$k];
      }
      $exists->save();
      return $exists->invoice_id;
    }
    else{
      $new = new self();
      foreach($key as $k){
        $new->$k = $data[$k];
      }
      $new->save();

      return $new->invoice_id;
    }
  }

  public static function updateData($data)
  {
    self::where('invoice_id', $data['invoice_id'])->update($data);
  }
}
