<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiketJenis extends Model
{
  protected $table = 'tiket_jenis';
  protected $primaryKey = 'tiket_jenis_id';

  public const ID_PENGADUAN = '1';
  public const ID_PENGEMBANGAN = '2';

  public const PENGADUAN = 'Pengaduan';
  public const PENGEMBANGAN = 'Pengembangan';

  public static $jenis = [
    self::PENGADUAN, self::PENGEMBANGAN
  ];

  public static function getData()
  {
    return self::all();
  }

  public static function getNama($id)
  {
    if($id == 'all') return 'Semua Jenis Layanan';
    $get = self::where('tiket_jenis_id', $id)->first();
    return $get ? $get->nama : null;
  }
}
