<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tiket extends Model
{
  protected $table = 'tiket';
  protected $primaryKey = 'tiket_id';

  public static function getInfo($tiket_id)
  {
    return self::leftJoin('layanan as l','l.layanan_id','=','tiket.layanan_id')
      ->leftJoin('tiket_jenis as j','j.tiket_jenis_id','=','tiket.tiket_jenis_id')
      ->leftJoin('tiket_foto as f','f.tiket_id','=','tiket.tiket_id')
      ->leftJoin('tiket_status as s','s.tiket_id' ,'=','tiket.tiket_id')
      ->leftJoin('tiket_pegawai as tp','tp.tiket_id' ,'=','tiket.tiket_id')
      ->leftJoin('users as u','u.username','=','tp.nama')
      ->leftJoin('pegawai as p','p.user_id','=','u.user_id')
      ->leftJoin('klien as k','k.klien_id','=','tiket.klien_id')
      ->leftJoin('tiket_status_diproses as sd','sd.tiket_id','=','tiket.tiket_id')
      ->where('tiket.tiket_id', $tiket_id)
      ->select('tiket.*','f.url','s.nama as status','j.nama as tiket_jenis','p.nama as pegawai','p.user_id as user_id_pegawai',
        'l.nama as nama_layanan','k.nama as nama_klien','k.user_id as user_id_klien','k.no_hp','k.alamat',
        'sd.progress','sd.foto as foto_progress','sd.keterangan as keterangan_progress','sd.created_at as waktu_progress')
      ->first();
  }

  public static function getDataMenunggu($tiket_jenis_id, $layanan_id, $klien_id = null)
  {
    $get = self::leftJoin('layanan as l','l.layanan_id','=','tiket.layanan_id')
      ->leftJoin('tiket_jenis as j','j.tiket_jenis_id','=','tiket.tiket_jenis_id')
      ->leftJoin('tiket_foto as f','f.tiket_id','=','tiket.tiket_id')
      ->leftJoin('tiket_status as s','s.tiket_id' ,'=','tiket.tiket_id')
      ->leftJoin('tiket_pegawai as p','p.tiket_id' ,'=','tiket.tiket_id')
      ->where('j.tiket_jenis_id', $tiket_jenis_id)
      ->where('l.layanan_id', $layanan_id);

    if($klien_id != null){
      $get = $get->where('tiket.klien_id', $klien_id);
    }

    return $get->where('s.nama', TiketStatus::S_MENUNGGU)
      ->select('tiket.*','f.url','s.nama as status','j.nama as tiket_jenis','p.nama as pegawai')
      ->get();
  }

  public static function getDataDiproses($tiket_jenis_id, $layanan_id, $klien_id = null)
  {
    $get = self::leftJoin('layanan as l','l.layanan_id','=','tiket.layanan_id')
      ->leftJoin('tiket_jenis as j','j.tiket_jenis_id','=','tiket.tiket_jenis_id')
      ->leftJoin('tiket_foto as f','f.tiket_id','=','tiket.tiket_id')
      ->leftJoin('tiket_status as s','s.tiket_id' ,'=','tiket.tiket_id')
      ->leftJoin('tiket_pegawai as p','p.tiket_id' ,'=','tiket.tiket_id')
      ->leftJoin('tiket_status_diproses as sd','sd.tiket_id','=','tiket.tiket_id')
      ->leftJoin('klien as k','k.klien_id','=','tiket.klien_id')
      ->where('j.tiket_jenis_id', $tiket_jenis_id)
      ->where('l.layanan_id', $layanan_id);

    if($klien_id != null){
      $get = $get->where('tiket.klien_id', $klien_id);
    }

    return $get->where('s.nama', TiketStatus::S_DIPROSES)
      ->select('tiket.*','f.url','s.nama as status','j.nama as tiket_jenis','p.nama as pegawai','sd.progress','k.nama as nama_klien')
      ->get();
  }

  public static function getDataDiprosesSelesaiAll()
  {
    return self::leftJoin('tiket_status as s','s.tiket_id' ,'=','tiket.tiket_id')
      ->leftJoin('tiket_pegawai as tp','tp.tiket_id' ,'=','tiket.tiket_id')
      ->leftJoin('users as u','u.username','=','tp.nama')
      ->leftJoin('pegawai as p','p.user_id','=','u.user_id')
      ->whereIn('s.nama', [TiketStatus::S_DIPROSES, TiketStatus::S_SELESAI])
      ->select('tiket.*','s.nama as status','p.pegawai_id')
      ->get();
  }

  public static function getDataSelesai($tiket_jenis_id, $layanan_id, $klien_id = null)
  {
    $get = self::leftJoin('layanan as l','l.layanan_id','=','tiket.layanan_id')
      ->leftJoin('tiket_jenis as j','j.tiket_jenis_id','=','tiket.tiket_jenis_id')
      ->leftJoin('tiket_foto as f','f.tiket_id','=','tiket.tiket_id')
      ->leftJoin('tiket_status as s','s.tiket_id' ,'=','tiket.tiket_id')
      ->leftJoin('tiket_pegawai as p','p.tiket_id' ,'=','tiket.tiket_id')
      ->leftJoin('tiket_status_diproses as sd','sd.tiket_id','=','tiket.tiket_id')
      ->leftJoin('klien as k','k.klien_id','=','tiket.klien_id')
      ->where('j.tiket_jenis_id', $tiket_jenis_id)
      ->where('l.layanan_id', $layanan_id);

    if($klien_id != null){
      $get = $get->where('tiket.klien_id', $klien_id);
    }

    return $get->where('s.nama', TiketStatus::S_SELESAI)
      ->select('tiket.*','f.url','s.nama as status','j.nama as tiket_jenis','p.nama as pegawai','sd.progress','k.nama as nama_klien')
      ->get();
  }

  public static function getArsip($val)
  {
    $get = self::leftJoin('layanan as l','l.layanan_id','=','tiket.layanan_id')
      ->leftJoin('tiket_jenis as j','j.tiket_jenis_id','=','tiket.tiket_jenis_id')
      ->leftJoin('tiket_status as s','s.tiket_id' ,'=','tiket.tiket_id')
      ->leftJoin('tiket_pegawai as tp','tp.tiket_id' ,'=','tiket.tiket_id')
      ->leftJoin('users as u','u.username','=','tp.nama')
      ->leftJoin('pegawai as p','p.user_id','=','u.user_id')
      ->leftJoin('klien as k','k.klien_id','=','tiket.klien_id')
      ->leftJoin('tagihan as t','t.tiket_id','=','tiket.tiket_id')
      ->leftJoin('tiket_foto as f','f.tiket_id','=','tiket.tiket_id');

    if($val['date_option'] == 'bulan' && $val['bulan'] != 'all'){
      $get = $get->where('tiket.created_at','like', $val['bulan'].'%');
    }
    if($val['date_option'] == 'tanggal'){
      $get = $get->where('tiket.created_at','like', $val['tanggal'].'%');
    }
    if($val['tiket_jenis_id'] != 'all'){
      $get = $get->where('tiket.tiket_jenis_id', $val['tiket_jenis_id']);
    }
    if($val['layanan_id'] != 'all'){
      $get = $get->where('tiket.layanan_id', $val['layanan_id']);
    }
    if($val['status'] != 'all'){
      $get = $get->where('s.nama', $val['status']);
    }
    if($val['pegawai_id'] != 'all'){
      $get = $get->where('p.pegawai_id', $val['pegawai_id']);
    }
    if($val['status_bayar'] != 'all'){
      if($val['status_bayar'] == Tagihan::S_BERBAYAR){
        $get = $get->where('t.pembayaran', '!=', null);
      }
      else{
        $get = $get->where('t.pembayaran', null);
      }
    }

    return $get->select('tiket.*','f.url','s.nama as status','j.nama as tiket_jenis','p.nama as pegawai',
      'k.nama as nama_klien','l.nama as nama_layanan','t.pembayaran','t.date','t.due_date','t.sisa_pembayaran')
      ->get();
  }

  public static function getArrBulan()
  {
    return self::selectRaw("SUBSTRING(created_at, 1, 7) as bulan")
      ->groupBy('bulan')
      ->orderBy('bulan','DESC')
      ->pluck('bulan')
      ->toArray();
  }

  public static function getArrTahun()
  {
    return self::selectRaw("SUBSTRING(created_at, 1, 4) as tahun")
      ->groupBy('tahun')
      ->orderBy('tahun','DESC')
      ->pluck('tahun')
      ->toArray();
  }

  public static function getJumlahPengaduan()
  {
    return self::where('tiket_jenis_id', TiketJenis::ID_PENGADUAN)
      ->count();
  }

  public static function getJumlahPengembangan()
  {
    return self::where('tiket_jenis_id', TiketJenis::ID_PENGEMBANGAN)
      ->count();
  }

  public static function getJumlahPengaduanByBulan($bulan)
  {
    return self::where('created_at', 'like', $bulan.'%')
      ->where('tiket_jenis_id', TiketJenis::ID_PENGADUAN)
      ->count();
  }

  public static function getJumlahPengembanganByBulan($bulan)
  {
    return self::where('created_at', 'like', $bulan.'%')
      ->where('tiket_jenis_id', TiketJenis::ID_PENGEMBANGAN)
      ->count();
  }

  public static function getJumlahByLayanan($tahun, $layanan_id)
  {
    return self::where('created_at', 'like', $tahun.'%')
      ->where('layanan_id', $layanan_id)
      ->count();
  }

  public static function insertData($data)
  {
    $key = array_keys($data);
    $new = new self();
    foreach($key as $k){
      $new->$k = $data[$k];
    }
    $new->save();

    return $new->tiket_id;
  }
}
