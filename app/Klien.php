<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Klien extends Model
{
  protected $table = 'klien';
  protected $primaryKey = 'klien_id';

  public static function getKlienId()
  {
    $user_id = Auth::user()->user_id;
    $get = self::where('user_id', $user_id)->first();

    return $get ? $get->klien_id : null;
  }

  public static function getJumlah()
  {
    return self::count();
  }
}
