<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
  protected $table = 'review';
  protected $primaryKey = 'review_id';

  public const LIKE = 'like';
  public const DISLIKE = 'dislike';

  public static function getInfo($tiket_id)
  {
    return self::where('tiket_id', $tiket_id)->first();
  }

  public static function insertData($data)
  {
    $key = array_keys($data);
    $new = new self();
    foreach($key as $k){
      $new->$k = $data[$k];
    }
    $new->save();

    return $new->review_id;
  }
}
