<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiketPegawai extends Model
{
  protected $table = 'tiket_pegawai';
  protected $primaryKey = 'tiket_pegawai_id';

  public const DEFAULT_PEGAWAI = 'teknisi1';

  public static function insertData($data)
  {
    $key = array_keys($data);
    $new = new self();
    foreach($key as $k){
      $new->$k = $data[$k];
    }
    $new->save();

    return $new->tiket_pegawai_id;
  }

  public static function updateData($data)
  {
    self::where('tiket_id', $data['tiket_id'])->update($data);
  }
}
