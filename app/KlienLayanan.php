<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KlienLayanan extends Model
{
  protected $table = 'klien_layanan';

  public static function getByKlienId($klien_id)
  {
    return self::join('klien','klien.klien_id','=','klien_layanan.klien_id')
      ->join('layanan', 'layanan.layanan_id', '=', 'klien_layanan.layanan_id')
      ->select('layanan.layanan_id', 'layanan.nama', 'layanan.jenis')
      ->where('klien.klien_id', $klien_id)
      ->get();
  }
}
