<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiketFoto extends Model
{
  protected $table = 'tiket_foto';

  public static function insertData($data)
  {
    $key = array_keys($data);
    $new = new self();
    foreach($key as $k){
      $new->$k = $data[$k];
    }
    $new->save();

    return $new->id;
  }
}
