<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PegawaiLayanan extends Model
{
  protected $table = 'pegawai_layanan';

  public static function getByPegawaiId($pegawai_id)
  {
    return self::leftJoin('pegawai as p', 'p.pegawai_id', '=', 'pegawai_layanan.pegawai_id')
      ->leftJoin('layanan as l', 'l.layanan_id', '=', 'pegawai_layanan.layanan_id')
      ->select('l.layanan_id', 'l.nama')
      ->where('pegawai_layanan.pegawai_id', $pegawai_id)
      ->get();
  }
}
