<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
  protected $table = 'comment';
  protected $primaryKey = 'comment_id';

  public static function getData($tiket_id)
  {
    return self::leftJoin('users as u','u.user_id','=','comment.user_id')
      ->leftJoin('klien as k','k.user_id','=','u.user_id')
      ->leftJoin('pegawai as p','p.user_id','=','u.user_id')
      ->where('comment.tiket_id', $tiket_id)
      ->select('comment.*','u.level','p.nama as nama_pegawai','k.nama as nama_klien')
      ->orderBy('comment.created_at','ASC')
      ->get();
  }

  public static function insertData($data)
  {
    $key = array_keys($data);
    $new = new self();
    foreach($key as $k){
      $new->$k = $data[$k];
    }
    $new->save();

    return $new->comment_id;
  }
}
