<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiketStatus extends Model
{
  protected $table = 'tiket_status';
  protected $primaryKey = 'tiket_status_id';

  public const S_MENUNGGU = 'menunggu';
  public const S_DIPROSES = 'diproses';
  public const S_SELESAI = 'selesai';

  public static $status = [
    self::S_MENUNGGU, self::S_DIPROSES, self::S_SELESAI,
  ];

  public static function getJumlahMenunggu($id_layanan, $jenis, $klien_id = null)
  {
    $get = self::join('tiket as t', 't.tiket_id', '=', 'tiket_status.tiket_id')
      ->join('klien as k', 'k.klien_id', '=', 't.klien_id')
      ->join('tiket_jenis as j','j.tiket_jenis_id','=','t.tiket_jenis_id');
    if($klien_id != null){
      $get = $get->where('k.klien_id', $klien_id);
    }
    return $get->where('t.layanan_id', $id_layanan)
      ->where('j.nama', $jenis)
      ->where('tiket_status.nama', self::S_MENUNGGU)
      ->count();
  }

  public static function getJumlahDiproses($id_layanan, $jenis, $klien_id = null)
  {
    $get = self::join('tiket as t', 't.tiket_id', '=', 'tiket_status.tiket_id')
      ->join('klien as k', 'k.klien_id', '=', 't.klien_id')
      ->join('tiket_jenis as j','j.tiket_jenis_id','=','t.tiket_jenis_id');
    if($klien_id != null){
      $get = $get->where('k.klien_id', $klien_id);
    }
    return $get->where('t.layanan_id', $id_layanan)
      ->where('j.nama', $jenis)
      ->where('tiket_status.nama', self::S_DIPROSES)
      ->count();
  }

  public static function getJumlahSelesai($id_layanan, $jenis, $klien_id = null)
  {
    $get = self::join('tiket as t', 't.tiket_id', '=', 'tiket_status.tiket_id')
      ->join('klien as k', 'k.klien_id', '=', 't.klien_id')
      ->join('tiket_jenis as j','j.tiket_jenis_id','=','t.tiket_jenis_id');
    if($klien_id != null){
      $get = $get->where('k.klien_id', $klien_id);
    }
    return $get->where('t.layanan_id', $id_layanan)
      ->where('j.nama', $jenis)
      ->where('tiket_status.nama', self::S_SELESAI)
      ->count();
  }

  public static function insertData($data)
  {
    $key = array_keys($data);
    $new = new self();
    foreach($key as $k){
      $new->$k = $data[$k];
    }
    $new->save();

    return $new->tiket_status_id;
  }

  public static function updateData($data)
  {
    self::where('tiket_id', $data['tiket_id'])->update($data);
  }
}
