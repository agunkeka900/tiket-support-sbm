<?php

namespace App\Http\Controllers;

use App\Tagihan;
use App\Tiket;
use Illuminate\Http\Request;

class TagihanController extends Controller
{
  public function index()
  {
    $data = Tagihan::getData();

    return view('pages.tagihan.index')
      ->with('data', $data);
  }

  public function viewEdit($tiket_id)
  {
    $data = Tagihan::getInfo($tiket_id);

    return view('pages.tagihan.edit')
      ->with('data', $data);
  }

  public function store(Request $req)
  {
    Tagihan::insertData([
      'date' => $req->date,
      'due_date' => $req->due_date,
      'tiket_id' => $req->tiket_id,
      'klien_id' => $req->klien_id,
      'pembayaran' => $req->pembayaran,
      'sisa_pembayaran' => $req->pembayaran
    ]);

    NotifikasiController::tagihan($req->tiket_id);

    return back()->with('success','Berhasil menyimpan data');
  }

  public function update(Request $req)
  {
    Tagihan::updateData([
      'invoice_id' => $req->invoice_id,
      'pembayaran' => $req->pembayaran,
      'sisa_pembayaran' => $req->sisa_pembayaran,
      'date' => $req->date,
      'due_date' => $req->due_date,
    ]);

    return back()->with('success','Berhasil menyimpan data');
  }

  public function cetak($tiket_id)
  {
    $data = [
      'data' => Tagihan::getInfo($tiket_id),
      'data_tiket' => Tiket::getInfo($tiket_id)
    ];

    $pdf = \App::make('dompdf.wrapper');
    $pdf->setPaper('a5', 'portrait');
    $pdf->loadView('pdf.tagihan', $data);
    return $pdf->stream('Tagihan.pdf');
  }
}
