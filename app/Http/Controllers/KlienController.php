<?php

namespace App\Http\Controllers;

use App\Klien;
use App\KlienLayanan;
use App\Layanan;
use App\PicHistory;
use App\Review;
use App\Tagihan;
use App\Tiket;
use App\TiketJenis;
use App\TiketStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KlienController extends Controller
{
  public function index()
  {
    $klien_id = Klien::getKlienId();
    $tiket_jenis = TiketJenis::getData();
    $layanan = KlienLayanan::getByKlienId($klien_id);
    $layanan = $this->setJumlahStatus($layanan, $tiket_jenis, $klien_id);
//    dd($layanan);

    return view('pages.dashboard.klien')
      ->with('tiket_jenis', $tiket_jenis)
      ->with('layanan', $layanan);
  }

  private function setJumlahStatus($layanan, $jenis, $klien_id)
  {
    foreach($layanan as $i=>$l){
      $temp = [];
      foreach($jenis as $j){
        $temp['menunggu'] = TiketStatus::getJumlahMenunggu($l->layanan_id, $j->nama, $klien_id);
        $temp['diproses'] = TiketStatus::getJumlahDiproses($l->layanan_id, $j->nama, $klien_id);
        $temp['selesai'] = TiketStatus::getJumlahSelesai($l->layanan_id, $j->nama, $klien_id);

        $layanan[$i][$j->nama] = $temp;
      }
    }

    return $layanan;
  }

  public function viewMenunggu($tiket_jenis_id, $layanan_id)
  {
    $klien_id = Klien::getKlienId();
    $data = Tiket::getDataMenunggu($tiket_jenis_id, $layanan_id, $klien_id);
    $nama_layanan = Layanan::getNama($layanan_id);
    $nama_jenis = TiketJenis::getNama($tiket_jenis_id);
//    dd($data);

    return view('pages.pengaduan.menunggu')
      ->with('data', $data)
      ->with('nama_layanan', $nama_layanan)
      ->with('nama_jenis', $nama_jenis);
  }

  public function viewDiproses($tiket_jenis_id, $layanan_id)
  {
    $klien_id = Klien::getKlienId();
    $data = Tiket::getDataDiproses($tiket_jenis_id, $layanan_id, $klien_id);
    $nama_layanan = Layanan::getNama($layanan_id);
    $data_status = TiketStatus::$status;
    $nama_jenis = TiketJenis::getNama($tiket_jenis_id);

    return view('pages.pengaduan.diproses_klien')
      ->with('data', $data)
      ->with('tiket_jenis_id', $tiket_jenis_id)
      ->with('data_status', $data_status)
      ->with('nama_layanan', $nama_layanan)
      ->with('nama_jenis', $nama_jenis);
  }

  public function viewDiprosesDetail($tiket_jenis_id, $tiket_id)
  {
    $data_comment = CommentController::getData($tiket_id);
    $data_tiket = Tiket::getInfo($tiket_id);
    $data_tagihan = Tagihan::getInfo($tiket_id);
    $data_history_pic = PicHistory::getData($tiket_id);

    return view('pages.pengaduan.detail_klien')
      ->with('back_url', 'diproses')
      ->with('data_comment', $data_comment)
      ->with('data_tagihan', $data_tagihan)
      ->with('data_tiket', $data_tiket)
      ->with('data_history_pic', $data_history_pic);
  }

  public function viewSelesai($tiket_jenis_id, $layanan_id)
  {
    $klien_id = Klien::getKlienId();
    $data = Tiket::getDataSelesai($tiket_jenis_id, $layanan_id, $klien_id);
    $nama_layanan = Layanan::getNama($layanan_id);
    $nama_jenis = TiketJenis::getNama($tiket_jenis_id);

    return view('pages.pengaduan.selesai_klien')
      ->with('data', $data)
      ->with('tiket_jenis_id', $tiket_jenis_id)
      ->with('nama_layanan', $nama_layanan)
      ->with('nama_jenis', $nama_jenis);
  }

  public function viewSelesaiDetail($tiket_jenis_id, $tiket_id)
  {
    $data_comment = CommentController::getData($tiket_id);
    $data_tiket = Tiket::getInfo($tiket_id);
    $data_tagihan = Tagihan::getInfo($tiket_id);
    $data_review = Review::getInfo($tiket_id);
    $data_history_pic = PicHistory::getData($tiket_id);
//    dd($data_review);

    return view('pages.pengaduan.detail_klien')
      ->with('back_url', 'selesai')
      ->with('data_comment', $data_comment)
      ->with('data_tagihan', $data_tagihan)
      ->with('data_tiket', $data_tiket)
      ->with('data_review', $data_review)
      ->with('data_history_pic', $data_history_pic);
  }
}
