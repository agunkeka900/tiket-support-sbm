<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Klien;
use App\Layanan;
use App\Pegawai;
use App\PegawaiLayanan;
use App\PicHistory;
use App\Review;
use App\Tagihan;
use App\Tiket;
use App\TiketJenis;
use App\TiketStatus;
use Illuminate\Http\Request;

class PegawaiController extends Controller
{
  public function index()
  {
    $val = HelperController::getValue('dashboard', ['tahun']);
    $data_tahun = Tiket::getArrTahun();
    $tahun =  $val['tahun'] != null ? $val['tahun'] : (count($data_tahun) ? $data_tahun[0] : date('Y'));
    $data_jumlah = $this->getJumlah();
    $chart_pengaduan = $this->getChartJumlahPengaduan($tahun);
    $chart_layanan = $this->getChartLayanan($tahun);

    return view('pages.dashboard.pegawai')
      ->with('tahun', $tahun)
      ->with('data_jumlah', $data_jumlah)
      ->with('data_tahun', $data_tahun)
      ->with('chart_pengaduan', $chart_pengaduan)
      ->with('chart_layanan', $chart_layanan);
  }

  private function getJumlah()
  {
    $pengaduan = Tiket::getJumlahPengaduan();
    $pengembangan = Tiket::getJumlahPengembangan();
    $klien = Klien::getJumlah();
    $tagihan = Tagihan::getTotal();

    return [
      'pengaduan' => $pengaduan,
      'pengembangan' => $pengembangan,
      'klien' => $klien,
      'tagihan' => $tagihan
    ];
  }

  public function getChartJumlahPengaduan($tahun)
  {
    $data = [
      'bulan' => HelperController::$arr_bulan,
      'pengaduan' => [
        'label' => 'Jumlah Pengaduan',
        'backgroundColor' => '#00A65A',
        'data' => []
      ],
      'pengembangan' => [
        'label' => 'Jumlah Pengembangan',
        'backgroundColor' => '#F39C12',
        'data' => []
      ]
    ];

    for($i=1; $i<=12; $i++){
      $bulan = $tahun.'-'.(strlen($i) == 1 ? sprintf("%02d", $i) : $i);
      $data['pengaduan']['data'][] = Tiket::getJumlahPengaduanByBulan($bulan);
      $data['pengembangan']['data'][] = Tiket::getJumlahPengembanganByBulan($bulan);
    }

//    dd($data);
    return $data;
  }

  public function getChartLayanan($tahun)
  {
    $get = Layanan::getData();
    $nama = [];
    $jumlah = [];
    foreach($get as $g){
      $nama[] = $g['nama'];
      $jumlah[] = Tiket::getJumlahByLayanan($tahun, $g['layanan_id']);
    }

    return [
      'nama' => $nama,
      'jumlah' => $jumlah
    ];
  }

  public function viewPengaduan()
  {
    $pegawai_id = Pegawai::getPegawaiId();
    $layanan = PegawaiLayanan::getByPegawaiId($pegawai_id);
    $layanan = $this->setJumlahStatus($layanan, TiketJenis::PENGADUAN);

    return view('pages.pengaduan.index_pegawai')
      ->with('layanan', $layanan);
  }

  public function viewPengembangan()
  {
    $pegawai_id = Pegawai::getPegawaiId();
    $layanan = PegawaiLayanan::getByPegawaiId($pegawai_id);
    $layanan = $this->setJumlahStatus($layanan, TiketJenis::PENGEMBANGAN);

    return view('pages.pengembangan.index_pegawai')
      ->with('layanan', $layanan);
  }

  private function setJumlahStatus($layanan, $jenis)
  {
    foreach($layanan as $i=>$l){
      $layanan[$i]['menunggu'] = TiketStatus::getJumlahMenunggu($l->layanan_id, $jenis);
      $layanan[$i]['diproses'] = TiketStatus::getJumlahDiproses($l->layanan_id, $jenis);
      $layanan[$i]['selesai'] = TiketStatus::getJumlahSelesai($l->layanan_id, $jenis);
    }

    return $layanan;
  }

  public function viewPengaduanMenunggu($layanan_id)
  {
    $data = Tiket::getDataMenunggu(TiketJenis::ID_PENGADUAN, $layanan_id);
    $nama_layanan = Layanan::getNama($layanan_id);
    $data_status = TiketStatus::$status;
//    dd($data);

    return view('pages.pengaduan.menunggu_pegawai')
      ->with('data', $data)
      ->with('data_status', $data_status)
      ->with('nama_layanan', $nama_layanan);
  }

  public function viewPengembanganMenunggu($layanan_id)
  {
    $data = Tiket::getDataMenunggu(TiketJenis::ID_PENGEMBANGAN, $layanan_id);
    $nama_layanan = Layanan::getNama($layanan_id);
    $data_status = TiketStatus::$status;
//    dd($data);

    return view('pages.pengembangan.menunggu_pegawai')
      ->with('data', $data)
      ->with('data_status', $data_status)
      ->with('nama_layanan', $nama_layanan);
  }

  public function viewPengaduanDiproses($layanan_id)
  {
    $data = Tiket::getDataDiproses(TiketJenis::ID_PENGADUAN, $layanan_id);
    $nama_layanan = Layanan::getNama($layanan_id);
    $data_status = TiketStatus::$status;
//    dd($data);

    return view('pages.pengaduan.diproses_pegawai')
      ->with('data', $data)
      ->with('data_status', $data_status)
      ->with('nama_layanan', $nama_layanan);
  }

  public function viewPengembanganDiproses($layanan_id)
  {
    $data = Tiket::getDataDiproses(TiketJenis::ID_PENGEMBANGAN, $layanan_id);
    $nama_layanan = Layanan::getNama($layanan_id);
    $data_status = TiketStatus::$status;
//    dd($data);

    return view('pages.pengembangan.diproses_pegawai')
      ->with('data', $data)
      ->with('data_status', $data_status)
      ->with('nama_layanan', $nama_layanan);
  }

  public function viewPengaduanDiprosesDetail($tiket_id)
  {
    $data_comment = CommentController::getData($tiket_id);
    $data_tiket = Tiket::getInfo($tiket_id);
    $data_tagihan = Tagihan::getInfo($tiket_id);
    $data_history_pic = PicHistory::getData($tiket_id);
//    dd($data_tagihan);

    return view('pages.pengaduan.detail_pegawai')
      ->with('back_url', 'diproses')
      ->with('data_comment', $data_comment)
      ->with('data_tagihan', $data_tagihan)
      ->with('data_tiket', $data_tiket)
      ->with('data_history_pic', $data_history_pic);
  }

  public function viewPengembanganDiprosesDetail($tiket_id)
  {
    $data_comment = CommentController::getData($tiket_id);
    $data_tiket = Tiket::getInfo($tiket_id);
    $data_tagihan = Tagihan::getInfo($tiket_id);
    $data_history_pic = PicHistory::getData($tiket_id);
//    dd($data_tagihan);

    return view('pages.pengembangan.detail_pegawai')
      ->with('back_url', 'diproses')
      ->with('data_comment', $data_comment)
      ->with('data_tagihan', $data_tagihan)
      ->with('data_tiket', $data_tiket)
      ->with('data_history_pic', $data_history_pic);
  }

  public function viewPengaduanSelesai($layanan_id)
  {
    $data = Tiket::getDataSelesai(TiketJenis::ID_PENGADUAN, $layanan_id);
    $nama_layanan = Layanan::getNama($layanan_id);

    return view('pages.pengaduan.selesai_pegawai')
      ->with('data', $data)
      ->with('nama_layanan', $nama_layanan);
  }

  public function viewPengembanganSelesai($layanan_id)
  {
    $data = Tiket::getDataSelesai(TiketJenis::ID_PENGEMBANGAN, $layanan_id);
    $nama_layanan = Layanan::getNama($layanan_id);

    return view('pages.pengembangan.selesai_pegawai')
      ->with('data', $data)
      ->with('nama_layanan', $nama_layanan);
  }

  public function viewPengaduanSelesaiDetail($tiket_id)
  {
    $data_comment = CommentController::getData($tiket_id);
    $data_tiket = Tiket::getInfo($tiket_id);
    $data_tagihan = Tagihan::getInfo($tiket_id);
    $data_review = Review::getInfo($tiket_id);
    $data_history_pic = PicHistory::getData($tiket_id);
//    dd($data_tagihan);

    return view('pages.pengaduan.detail_pegawai')
      ->with('back_url', 'selesai')
      ->with('data_comment', $data_comment)
      ->with('data_tagihan', $data_tagihan)
      ->with('data_tiket', $data_tiket)
      ->with('data_review', $data_review)
      ->with('data_history_pic', $data_history_pic);
  }

  public function viewPengembanganSelesaiDetail($tiket_id)
  {
    $data_comment = CommentController::getData($tiket_id);
    $data_tiket = Tiket::getInfo($tiket_id);
    $data_tagihan = Tagihan::getInfo($tiket_id);
    $data_review = Review::getInfo($tiket_id);
    $data_history_pic = PicHistory::getData($tiket_id);
//    dd($data_tagihan);

    return view('pages.pengembangan.detail_pegawai')
      ->with('back_url', 'selesai')
      ->with('data_comment', $data_comment)
      ->with('data_tagihan', $data_tagihan)
      ->with('data_tiket', $data_tiket)
      ->with('data_review', $data_review)
      ->with('data_history_pic', $data_history_pic);
  }
}
