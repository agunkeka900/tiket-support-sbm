<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
  public function like(Request $req)
  {
    Review::insertData([
      'layanan_id' => $req->layanan_id,
      'klien_id' => $req->klien_id,
      'tiket_id' => $req->tiket_id,
      'review' => Review::LIKE
    ]);

    NotifikasiController::like($req->tiket_id);

    return back()->with('success','Berhasil memberikan review');
  }
  public function dislike(Request $req)
  {
    Review::insertData([
      'layanan_id' => $req->layanan_id,
      'klien_id' => $req->klien_id,
      'tiket_id' => $req->tiket_id,
      'review' => Review::DISLIKE
    ]);

    NotifikasiController::dislike($req->tiket_id);

    return back()->with('success','Berhasil memberikan review');
  }
}
