<?php

namespace App\Http\Controllers;

use App\Notifikasi;
use App\Tagihan;
use App\Tiket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotifikasiController extends Controller
{
  public function indexPegawai()
  {
    $data = Notifikasi::getNotifikasi();

    return view('pages.notifikasi.index_pegawai')
      ->with('data', $data);
  }

  public function indexKlien()
  {
    $data = Notifikasi::getNotifikasi();

    return view('pages.notifikasi.index_klien')
      ->with('data', $data);
  }

  public static function generateNotifId()
  {
    $string = md5(time());
    $id = substr($string, 0, 20);
    return strtoupper($id);
  }

  public static function pengaduanBaru($tiket_id)
  {
    $notif_id = self::generateNotifId();
    $pegawai = User::getUserPegawai();

    $info = Tiket::getInfo($tiket_id);
    $jenis = $info->tiket_jenis;
    $jenis_lowercase = strtolower($info->tiket_jenis);
    $nama_klien = $info->nama_klien;
    $judul = $info->judul;
    $layanan_id = $info->layanan_id;
    $nama_layanan = $info->nama_layanan;

    foreach($pegawai as $p){
      Notifikasi::insertData([
        'notif_id' => $notif_id,
        'user_id' => $p->user_id,
        'judul' => "$jenis Baru",
        'keterangan' => "Terdapat $jenis baru layanan $nama_layanan dari klien $nama_klien (judul: $judul)",
        'redirect_link' => "pegawai/$jenis_lowercase/menunggu/$layanan_id",
        'dibaca' => Notifikasi::D_BELUM
      ]);
    }
  }

  public static function prosesPIC($tiket_id)
  {
    $notif_id = self::generateNotifId();
    $info = Tiket::getInfo($tiket_id);
    $jenis_id = $info->tiket_jenis_id;
    $jenis = $info->tiket_jenis;
    $status = $info->status;
    $pegawai = $info->pegawai;
    $judul = $info->judul;

    Notifikasi::insertData([
      'notif_id' => $notif_id,
      'user_id' => $info->user_id_klien,
      'judul' => "$jenis $status",
      'keterangan' => "$jenis Anda ditangani oleh $pegawai (judul: $judul)",
      'redirect_link' => "klien/$status/detail/$jenis_id/$tiket_id",
      'dibaca' => Notifikasi::D_BELUM
    ]);
  }

  public static function updateProgress($tiket_id)
  {
    $notif_id = self::generateNotifId();
    $info = Tiket::getInfo($tiket_id);
    $jenis_id = $info->tiket_jenis_id;
    $jenis = $info->tiket_jenis;
    $status = $info->status;
    $judul = $info->judul;
    $progress = $info->progress;

    Notifikasi::insertData([
      'notif_id' => $notif_id,
      'user_id' => $info->user_id_klien,
      'judul' => "$jenis $status",
      'keterangan' => "Progress $jenis Anda $progress% (judul: $judul)",
      'redirect_link' => "klien/$status/detail/$jenis_id/$tiket_id",
      'dibaca' => Notifikasi::D_BELUM
    ]);
  }

  public static function selesaiProgress($tiket_id)
  {
    $notif_id = self::generateNotifId();
    $info = Tiket::getInfo($tiket_id);
    $jenis_id = $info->tiket_jenis_id;
    $jenis = $info->tiket_jenis;
    $status = $info->status;
    $judul = $info->judul;

    Notifikasi::insertData([
      'notif_id' => $notif_id,
      'user_id' => $info->user_id_klien,
      'judul' => "$jenis $status",
      'keterangan' => "$jenis Anda telah selesai diproses, silahkan berikan review pada $jenis ini (judul: $judul)",
      'redirect_link' => "klien/$status/detail/$jenis_id/$tiket_id",
      'dibaca' => Notifikasi::D_BELUM
    ]);
  }

  public static function balasKomentar($tiket_id, $comment)
  {
    $is_teknisi = Auth::user()->level == User::L_TEKNISI;
    $notif_id = self::generateNotifId();
    $info = Tiket::getInfo($tiket_id);
    $jenis_id = $info->tiket_jenis_id;
    $jenis = $info->tiket_jenis;
    $jenis_lowercase = strtolower($info->tiket_jenis);
    $status = $info->status;
    $judul = $info->judul;
    $nama = $is_teknisi ? 'Teknisi' : 'Klien';

    Notifikasi::insertData([
      'notif_id' => $notif_id,
      'user_id' => $is_teknisi ? $info->user_id_klien : $info->user_id_pegawai,
      'judul' => "Komentar $jenis",
      'keterangan' => "$nama memberikan komentar pada $jenis $judul (komentar: $comment)",
      'redirect_link' => $is_teknisi ? "klien/$status/detail/$jenis_id/$tiket_id" : "pegawai/$jenis_lowercase/$status/detail/$tiket_id",
      'dibaca' => Notifikasi::D_BELUM
    ]);
  }

  public static function like($tiket_id)
  {
    $notif_id = self::generateNotifId();
    $info = Tiket::getInfo($tiket_id);
    $jenis = $info->tiket_jenis;
    $jenis_lowercase = strtolower($info->tiket_jenis);
    $status = $info->status;
    $judul = $info->judul;

    Notifikasi::insertData([
      'notif_id' => $notif_id,
      'user_id' => $info->user_id_pegawai,
      'judul' => "Review $jenis",
      'keterangan' => "Klien memberikan review like pada $jenis $judul",
      'redirect_link' => "pegawai/$jenis_lowercase/$status/detail/$tiket_id",
      'dibaca' => Notifikasi::D_BELUM
    ]);
  }

  public static function dislike($tiket_id)
  {
    $notif_id = self::generateNotifId();
    $info = Tiket::getInfo($tiket_id);
    $jenis = $info->tiket_jenis;
    $jenis_lowercase = strtolower($info->tiket_jenis);
    $status = $info->status;
    $judul = $info->judul;

    Notifikasi::insertData([
      'notif_id' => $notif_id,
      'user_id' => $info->user_id_pegawai,
      'judul' => "Review $jenis",
      'keterangan' => "Klien memberikan review dislike pada $jenis $judul",
      'redirect_link' => "pegawai/$jenis_lowercase/$status/detail/$tiket_id",
      'dibaca' => Notifikasi::D_BELUM
    ]);
  }

  public static function tagihan($tiket_id)
  {
    $notif_id = self::generateNotifId();
    $info = Tiket::getInfo($tiket_id);
    $jenis = $info->tiket_jenis;
    $jenis_id = $info->tiket_jenis_id;
    $status = $info->status;
    $judul = $info->judul;

    $info_tagihan = Tagihan::getInfo($tiket_id);
    $jumlah_tagihan = "Rp ".number_format($info_tagihan->pembayaran, 0, ',', '.');
    $due_date = HelperController::setNamaBulan(null, $info_tagihan->due_date);

    Notifikasi::insertData([
      'notif_id' => $notif_id,
      'user_id' => $info->user_id_klien,
      'judul' => "Tagihan $jenis",
      'keterangan' => "Terdapat tagihan pada $jenis $judul (jumlah tagihan: $jumlah_tagihan) silahkan lakukan pembayaran sebelum tanggal $due_date",
      'redirect_link' => "klien/$status/detail/$jenis_id/$tiket_id",
      'dibaca' => Notifikasi::D_BELUM
    ]);
  }

  public function read($notif_id)
  {
    Notifikasi::read($notif_id);

    if(isset($_GET['redirect_link'])){
      return redirect($_GET['redirect_link']);
    }
    else return back();
  }

  public function readAll()
  {
    $user_id = Auth::user()->user_id;
    Notifikasi::readAll($user_id);

    return back();
  }

  public function delete(Request $req)
  {
    Notifikasi::deleteData($req->notif_id, $req->user_id);

    return back()->with('success','Berhasil menghapus notifikasi');
  }
}
