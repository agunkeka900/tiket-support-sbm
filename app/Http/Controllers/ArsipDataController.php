<?php

namespace App\Http\Controllers;

use App\Layanan;
use App\Pegawai;
use App\PicHistory;
use App\Tagihan;
use App\Tiket;
use App\TiketJenis;
use App\TiketStatus;
use Illuminate\Http\Request;

class ArsipDataController extends Controller
{
  public function index()
  {
    $val = HelperController::getValue('arsip_', ['date_option','bulan','tanggal','tiket_jenis_id','layanan_id','status','pegawai_id','status_bayar','pic']);
    $val['date_option'] = $val['date_option'] == null ? 'bulan' : $val['date_option'];
    $val['tanggal'] = $val['tanggal'] == null ? date('Y-m-d') : $val['tanggal'];
    $val['pic'] = $val['pic'] == null ? 'all' : $val['pic'];
    $data_bulan = Tiket::getArrBulan();
    $data_jenis_layanan = TiketJenis::getData();
    $data_status = TiketStatus::$status;
    $data_pegawai = Pegawai::getData();
    $data_layanan = Layanan::getData();
    $data_status_bayar = Tagihan::$status;
    $data = $this->getArsip($val);

    return view('pages.arsip.index')
      ->with('data_bulan', $data_bulan)
      ->with('data_jenis_layanan', $data_jenis_layanan)
      ->with('data_status', $data_status)
      ->with('data_pegawai', $data_pegawai)
      ->with('data_layanan', $data_layanan)
      ->with('data_status_bayar', $data_status_bayar)
      ->with('data', $data)
      ->with('date_option', $val['date_option'])
      ->with('bulan', $val['bulan'])
      ->with('tanggal', $val['tanggal'])
      ->with('tiket_jenis_id', $val['tiket_jenis_id'])
      ->with('layanan_id', $val['layanan_id'])
      ->with('status', $val['status'])
      ->with('pegawai_id', $val['pegawai_id'])
      ->with('status_bayar', $val['status_bayar'])
      ->with('pic', $val['pic']);
  }

  private function getArsip($val)
  {
    $get = Tiket::getArsip($val);

    if($val['pic'] == 'all') return $get;
    else{
      $data = [];

      foreach($get as $index=>$g){
        $pic = PicHistory::getData($g['tiket_id']);
        $jml_pic = $pic ? count($pic) : 0;
        if($val['pic'] == 0 && $jml_pic == 1){
          $g['history_pic'] = $pic;
          $data[] = $g;
        }
        elseif($val['pic'] != 0){
          if($jml_pic >= $val['pic']){
            $g['history_pic'] = $pic;
            $data[] = $g;
          }
        }
      }

//      dd($data);
      return $data;
    }
  }

  public function cetak()
  {
    $val = HelperController::getValue('arsip_', ['date_option','bulan','tanggal','tiket_jenis_id','layanan_id','status','pegawai_id','status_bayar','pic']);
    $data = [
      'data' => $this->getArsip($val),
      'date_option' => $val['date_option'],
      'bulan' => HelperController::setNamaBulan($val['bulan']),
      'tanggal' => HelperController::setNamaBulan(null, $val['tanggal']),
      'jenis_layanan' => TiketJenis::getNama($val['tiket_jenis_id']),
      'nama_layanan' => Layanan::getNama($val['layanan_id']),
      'pegawai' => Pegawai::getNama($val['pegawai_id']),
      'status_bayar' => $val['status_bayar'],
      'tiket_jenis_id' => $val['tiket_jenis_id'],
      'layanan_id' => $val['layanan_id'],
      'status' => $val['status'],
      'pegawai_id' => $val['pegawai_id'],
      'pic' => $val['pic'],
    ];

    $pdf = \App::make('dompdf.wrapper');
    $pdf->setPaper('a4', 'landscape');
    $pdf->loadView('pdf.arsip', $data);
    return $pdf->stream('Laporan Arsip.pdf');
  }
}
