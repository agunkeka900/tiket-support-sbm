<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class HelperController extends Controller
{
  public static $arr_bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
  public static function setNamaBulan($tahun_bulan, $tahun_bulan_tanggal = null)
  {
    $arr_nama_bulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];

    $exp = explode('-', $tahun_bulan_tanggal == null ? $tahun_bulan : $tahun_bulan_tanggal);

    if($tahun_bulan_tanggal == null && count($exp) < 2)
      return $tahun_bulan;
    elseif($tahun_bulan_tanggal != null && count($exp) < 3)
      return $tahun_bulan_tanggal;

    $tahun = $exp[0];
    $bulan = $exp[1];
    $nama_bulan = $arr_nama_bulan[(int)$bulan-1];

    return $tahun_bulan_tanggal == null ? $nama_bulan.' '.$tahun : $exp[2].' '.$nama_bulan.' '.$tahun;
  }

  public static function setNamaWaktu($Ymd_His)
  {
    $Ymd = explode(' ', $Ymd_His)[0];
    $Hi = substr($Ymd_His, 11, 5);
    $nama_bulan = self::setNamaBulan(null, $Ymd);

    return $nama_bulan.' - '.$Hi;
  }

  public static function isPdf($string)
  {
    if($string == null) return false;

    $a = explode('.', $string);
    return $a[count($a)-1] == 'pdf';
  }

  public static function isImage($string)
  {
    if($string == null) return false;

    $a = explode('.', $string);
    return in_array(strtolower($a[count($a)-1]), ['png','jpg','jpeg']);
  }

  public static function getValue($name, $arr, $arr_not_all = [])
  {
    $data = [];
    foreach($arr as $a){
      if(isset($_GET[$a])){
        $data[$a] = $_GET[$a];
      }
      else{
        $data[$a] = self::getCookie($name.$a) ?: null;
      }
    }
    $set_data = $data;

    foreach($arr_not_all as $an){
      if(isset($_GET[$an])){
        $value = $_GET[$an];
      }
      else{
        $value = self::getCookie($name.$an) ?: null;
      }
      $data[$an] = $value == 'all' ? null : $value;
      $set_data[$an] = $value;
    }

    self::setCookie($name, $set_data);
    return $data;
  }

  public static function setCookie($name, $data)
  {
    $user = 'user_'.Auth::user()->username;
    $arr = [];
    if(Cookie::get($user)){
      $cookie = Cookie::get($user);
      $arr = (array)json_decode($cookie);
    }
    foreach($data as $index=>$d){
      $arr[$name.$index] = $d;
    }
    Cookie::queue($user, json_encode($arr), 43200);
  }

  public static function getCookie($name)
  {
    $user = 'user_'.Auth::user()->username;
    if(Cookie::get($user)){
      $cookie = Cookie::get($user);
      $arr = (array)json_decode($cookie);
      if(isset($arr[$name])){
        return $arr[$name];
      }
    }
    return null;
  }

}
