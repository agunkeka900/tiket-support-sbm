<?php

namespace App\Http\Controllers;

use App\Comment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
  public static function getData($tiket_id)
  {
    $get = Comment::getData($tiket_id);
    $data = [];
    foreach($get as $g){
      $data[] = [
        'nama' => $g['level'] == User::L_TEKNISI ? $g['nama_pegawai'] : $g['nama_klien'],
        'level' => ucwords($g['level']),
        'comment' => $g['comment'],
        'lampiran' => $g['lampiran'],
        'waktu' => HelperController::setNamaWaktu($g['created_at']),
        'foto' => $g['level'] == User::L_TEKNISI ? 'profil-teknisi.jpg' : 'user4.png'
      ];
    }

    return $data;
  }

  public function store(Request $req)
  {
    $nama_file = $this->storeFile($req);
    Comment::insertData([
      'tiket_id' => $req->tiket_id,
      'comment' => $req->comment,
      'lampiran' => $nama_file,
      'user_id' => Auth::user()->user_id
    ]);

    NotifikasiController::balasKomentar($req->tiket_id, $req->comment);

    return back()->with('success','Berhasil mengirim komentar');
  }

  private function storeFile($req, $location = 'file_comment')
  {
    if($req->file('file')){
      $file = $req->file('file');
      $ext = $file->getClientOriginalExtension();
      $name = str_slug($req->tiket_id) .'-'. time() .'.' . $ext;
      $file->move(public_path() . "/$location", $name);

      return $location.'/'.$name;
    }
    else{
      return '-';
    }
  }

  private function deleteFile($nama, $location = 'foto_pengaduan')
  {
    File::delete("public/$location/$nama");
  }
}
