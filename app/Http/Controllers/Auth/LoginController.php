<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
  use AuthenticatesUsers;

  protected $redirectTo = '/home';

  public function __construct()
  {
    $this->middleware('guest')->except('logout');
  }

  public function username()
  {
    $login = request()->input('username');
    $field = 'username';
    request()->merge([$field => $login]);
    return $field;
  }

  protected function sendFailedLoginResponse(Request $request)
  {
    $errors = ['username' => ['Username / password tidak valid!']];
    return back()->withErrors($errors)->withInput();
  }

  protected function authenticated(Request $request, $user)
  {
    if($user->level == User::L_USER){
      return redirect('klien');
    }
    else return redirect('pegawai');
  }
}
