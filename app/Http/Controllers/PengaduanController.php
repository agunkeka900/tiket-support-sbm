<?php

namespace App\Http\Controllers;

use App\Klien;
use App\Pegawai;
use App\PicHistory;
use App\Tiket;
use App\TiketFoto;
use App\TiketPegawai;
use App\TiketStatus;
use App\TiketStatusDiproses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;

class PengaduanController extends Controller
{
  public function store(Request $req)
  {
    $klien_id = Klien::getKlienId();
    $tiket_id = Tiket::insertData([
      'layanan_id' => $req->layanan_id,
      'klien_id' => $klien_id,
      'judul' => $req->judul,
      'deskripsi' => $req->deskripsi,
      'tiket_jenis_id' => $req->tiket_jenis_id,
    ]);

    $nama_foto = $this->storeImageFile($req);
    if($nama_foto != null){
      TiketFoto::insertData([
        'tiket_id' => $tiket_id,
        'url' => $nama_foto
      ]);
    }

    TiketStatus::insertData([
      'nama' => TiketStatus::S_MENUNGGU,
      'tiket_id' => $tiket_id,
    ]);

    TiketPegawai::insertData([
      'nama' => TiketPegawai::DEFAULT_PEGAWAI,
      'tiket_id' => $tiket_id
    ]);

    NotifikasiController::pengaduanBaru($tiket_id);

    return back()->with('success','Berhasil meyimpan data');
  }

  public function updatePic(Request $req)
  {
    if($req->status == TiketStatus::S_DIPROSES){
      TiketStatusDiproses::insertIfNotExists([
        'tiket_id' => $req->tiket_id
      ]);
    }

    TiketStatus::updateData([
      'tiket_id' => $req->tiket_id,
      'nama' => $req->status
    ]);

    TiketPegawai::updateData([
      'tiket_id' => $req->tiket_id,
      'nama' => Auth::user()->username
    ]);

    PicHistory::insertData([
      'tiket_id' => $req->tiket_id,
      'pegawai_id' => Pegawai::getPegawaiId(),
      'created_at' => date('Y-m-d H:i:s'),
      'updated_at' => date('Y-m-d H:i:s'),
    ]);

    NotifikasiController::prosesPIC($req->tiket_id);

    return back()->with('success','Berhasil update PiC');
  }

  public function updateProgress(Request $req)
  {
    $info = TiketStatusDiproses::getInfo($req->tiket_id);
    $foto = $info->foto;
    if($foto != null){
      $this->deleteImageFile($info->foto);
      $foto = null;
    }
    if($req->file('foto')){
      $foto = $this->storeImageFile($req, 'foto_progress_pengaduan');
    }
    TiketStatusDiproses::updateData([
      'tiket_id' => $req->tiket_id,
      'progress' => (int)$req->progress,
      'foto' => $foto,
      'keterangan' => $req->keterangan
    ]);

    if($req->progress == 100){
      TiketStatus::updateData([
        'tiket_id' => $req->tiket_id,
        'nama' => TiketStatus::S_SELESAI
      ]);
      NotifikasiController::selesaiProgress($req->tiket_id);
      return back()->with('success','Berhasil menyelesaikan pengaduan');
    }
    else{
      NotifikasiController::updateProgress($req->tiket_id);
      return back()->with('success','Berhasil update progress');
    }
  }

  public function selesaiProgress(Request $req)
  {
    TiketStatusDiproses::updateData([
      'tiket_id' => $req->tiket_id,
      'progress' => 100
    ]);

    TiketStatus::updateData([
      'tiket_id' => $req->tiket_id,
      'nama' => TiketStatus::S_SELESAI
    ]);
    NotifikasiController::selesaiProgress($req->tiket_id);

    return back()->with('success','Berhasil menyelesaikan pengaduan');
  }

  private function storeImageFile($req, $location = 'foto_pengaduan')
  {
    if($req->file('foto')){
      $file = $req->file('foto');
      $ext = $file->getClientOriginalExtension();
      $name = str_slug($req->judul) .'-'. time() .'.' . $ext;
      $file->move(public_path() . "/$location", $name);

      return $location.'/'.$name;
    }
    else{
      return null;
    }
  }

  private function deleteImageFile($nama, $location = 'foto_pengaduan')
  {
    File::delete("$nama");
  }

  public function inisiasiPicHistory()
  {
    $get_tiket = Tiket::getDataDiprosesSelesaiAll();
    $get_pic = PicHistory::getDataAll();
    $data = [];

    foreach($get_tiket as $t){
      $pic = false;
      foreach($get_pic as $p){
        if($t['tiket_id'] == $p['tiket_id']) $pic = true;
      }
      if(!$pic) $data[] = $t;
    }

    foreach($data as $d){
      PicHistory::insertData([
        'tiket_id' => $d['tiket_id'],
        'pegawai_id' => $d['pegawai_id'],
        'created_at' => date('Y-m-d H:i:s'),
        'updated_at' => date('Y-m-d H:i:s'),
      ]);
    }

    dd("Berhasil inisiasi ".count($data).' data');
  }
}
