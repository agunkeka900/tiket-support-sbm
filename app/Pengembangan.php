<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengembangan extends Model
{
  protected $table = 'pengembangan';
  protected $primaryKey = 'pengembangan_id';
}
